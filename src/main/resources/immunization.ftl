<div class='card'>
    <header class='card-header'>
        <a href='#' data-toggle='collapse' data-target='#immunizations-body' aria-expanded='true' class=''>
            <i class='icon-action fa fa-chevron-down'></i>
        </a>
        <span class='title'>Vacunas</span>
    </header>
    <div class='card-body collapse show' id='immunizations-body'>
        <ul class='list-group'>
        <#list object.getVaccineCode().getCoding() as item>
        	<li class='list-group-item'>
                <span class='badge badge-primary'><#list object.getOccurrence().getExtension() as item2>${item2.getValue().getValue()}</#list></span>
                ${item.getDisplay()}
            </li>
        </#list>
        </ul>
    </div>
</div>