<div class='card'>
    <header class='card-header'>
        <a href='#' data-toggle='collapse' data-target='#medications-body' aria-expanded='true' class=''>
            <i class='icon-action fa fa-chevron-down'></i>
        </a>
        <span class='title'>Medicamentos activos</span>
    </header>
    <div class='card-body collapse show' id='medications-body'>
        <ul class='list-group'>
            <li class='list-group-item'>
	        <#list object.getMedication().getCoding() as item>
	            <span class='badge badge-primary'>${item.getSystem()}</span> ${item.getDisplay()} (${item.getCode()})<br/>
	        </#list>
        	</li>
        </ul>
    </div>
</div>