<#setting date_format='yyyy-MM-dd'>
<#setting locale='en_US'>

<div class='card'>
    <header class='card-header'>
        <a href='#' data-toggle='collapse' data-target='#patient-body' aria-expanded='true' class=''>
            <i class='icon-action fa fa-chevron-down'></i>
        </a>
        <span class='title'>Datos del Paciente </span>
    </header>
    <div class='card-body collapse show' id='patient-body'>
        <h5 class='card-title'><i class='fas fa-${object.getGender()}'></i>
            
            <#list object.getName() as item>${item.getFamily()}</#list>,
            <#list object.getName() as item>${item.getGivenAsSingleString()}</#list>
        </h5>
        <p class='card-text'>
            Fecha de Nacimiento: ${object.getBirthDate()?date}
            <br/>
            Contacto:
            
            <#list object.getTelecom() as item>
            <i class='fas fa-${item.getSystem()}'></i>${item.getValue()}
            </#list>
            
            <br/>
            Identificadores:<br/>
            <span class='small'>
                <#list object.getIdentifier() as item>
                ${item.getSystem()}:${item.getValue()}<br/>
                </#list>
            </span>
        </p>
    </div>
</div>