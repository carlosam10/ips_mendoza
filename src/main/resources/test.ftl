<div class="card">
    <header class="card-header">
        <a href="#" data-toggle="collapse" data-target="#problems-body" aria-expanded="true" class="">
            <i class="icon-action fa fa-chevron-down"></i>
        </a>
        <span class="title">Diagnósticos / Problemas activos </span>
    </header>
    <div class="card-body collapse show" id="problems-body">
        <ul class="list-group">
            <#list systems as system>
                <li>${system.name}</li>
            </#list>
        </ul>
    </div>
</div>

