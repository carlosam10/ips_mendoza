<#setting date_format='yyyy-MM-dd'>
<#setting locale='en_US'>

<div class='card'>
    <header class='card-header'>
        <a href='#' data-toggle='collapse' data-target='#problems-body' aria-expanded='true' class=''>
            <i class='icon-action fa fa-chevron-down'></i>
        </a>
        <span class='title'>Diagnósticos / Problemas activos</span>
    </header>
    <div class='card-body collapse show' id='problems-body'>
        <ul class='list-group'>        
        <#list object as item>
        	<#list item.getCode().getCoding() as coding>
	        	<li class='list-group-item'>
			        <span class='badge badge-primary'><#if item.getOnsetDateTimeType().getValue()??>${item.getOnsetDateTimeType().getValue()?date}<#else></#if></span>
			        <#if coding.getDisplay()??>${coding.getDisplay()}<#else>no posee descripción</#if> (<#if coding.getCode()??>${coding.getCode()}<#else>no posee código</#if>)
				</li>
        	</#list>
        </#list>        
        </ul>
    </div>
</div>