<div class='card'>
    <header class='card-header'>
        <a href='#' data-toggle='collapse' data-target='#Allergies-body' aria-expanded='true' class=''>
            <i class='icon-action fa fa-chevron-down'></i>
        </a>
        <span class='title'>Alergias e Intolerancias</span>
    </header>
    <div class='card-body collapse show' id='Allergies-body'>
        <ul class='list-group'>
            <li class='list-group-item'>   
            	<#list object.getCode().getCoding() as item>${item.getDisplay()} (${item.getCode()})<br/></#list>
            </li>        
        </ul>
    </div>
</div>