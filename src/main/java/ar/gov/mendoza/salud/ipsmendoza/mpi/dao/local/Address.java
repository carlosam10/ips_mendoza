
package ar.gov.mendoza.salud.ipsmendoza.mpi.dao.local;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties({"additionalProperties"})
public class Address {

    private String use;
    private String type;
    private List<String> line = new ArrayList<String>();
    private String city;
    private String district;
    private String state;
    private String country;
    private Period_ period;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Address() {
    }

    /**
     * 
     * @param state
     * @param line
     * @param use
     * @param period
     * @param district
     * @param type
     * @param country
     * @param city
     */
    public Address(String use, String type, List<String> line, String city, String district, String state, String country, Period_ period) {
        super();
        this.use = use;
        this.type = type;
        this.line = line;
        this.city = city;
        this.district = district;
        this.state = state;
        this.country = country;
        this.period = period;
    }

    public String getUse() {
        return use;
    }

    public void setUse(String use) {
        this.use = use;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getLine() {
        return line;
    }

    public void setLine(List<String> line) {
        this.line = line;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Period_ getPeriod() {
        return period;
    }

    public void setPeriod(Period_ period) {
        this.period = period;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
