package ar.gov.mendoza.salud.ipsmendoza.mpi.dao.builder;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hl7.fhir.r4.model.Bundle.BundleEntryComponent;
import org.hl7.fhir.utilities.xhtml.XhtmlNode;

import ar.gov.mendoza.salud.ipsmendoza.html.GenerateHTML;

import org.hl7.fhir.r4.model.AllergyIntolerance;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.CodeableConcept;
import org.hl7.fhir.r4.model.Coding;
import org.hl7.fhir.r4.model.Composition;
import org.hl7.fhir.r4.model.Composition.SectionComponent;
import org.hl7.fhir.r4.model.Condition;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Immunization;
import org.hl7.fhir.r4.model.Medication;
import org.hl7.fhir.r4.model.MedicationStatement;
import org.hl7.fhir.r4.model.Meta;
import org.hl7.fhir.r4.model.Narrative;
import org.hl7.fhir.r4.model.Reference;

public class CompositionBuilder implements Builder<Composition, Object> {

	private Identifier identifier;

	private Immunization immunization;
	private AllergyIntolerance allergyIntolerance;
	private MedicationStatement medication;
	private List<Condition> conditions;

	private final String[] meses = { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto",
			"Septiembre", "Octubre", "Noviembre", "Diciembre" };

	private final String path = "https://apisalud.mendoza.gov.ar/api/ips/fhir/";
	
	public CompositionBuilder(Identifier identifier, Immunization immunization, AllergyIntolerance allergyIntolerance,
			MedicationStatement medication, List<Condition> conditions) {
		this.identifier = identifier;
		this.immunization = immunization;
		this.allergyIntolerance = allergyIntolerance;
		this.medication = medication;
		this.conditions = conditions;
	}

	@Override
	public Composition construir(Identifier identifier) {
		/////////////// RECURSOS////////////////////////////////
		Composition composition = new Composition();

		composition.setId("1");
		CodeableConcept codeable = new CodeableConcept();

		Coding cod4 = new Coding();
		cod4.setCode("60591-5");
		cod4.setDisplay("Patient Summary");
		cod4.setSystem("http://loinc.org");

		codeable.addCoding(cod4);

		composition.setType(codeable);
		composition.setConfidentiality(Composition.DocumentConfidentiality.N);

		// TODO: Revisar implementación de fecha y hora
		LocalDate fechaActual = LocalDate.now();
		LocalTime horaActual = LocalTime.now();
		int dia = fechaActual.getDayOfMonth();
		String mes = meses[fechaActual.getMonthValue() - 1];
		int año = fechaActual.getYear();
		int hora = horaActual.getHour();
		int minuto = horaActual.getMinute();
		String actual = "Resumen del paciente al " + dia + " de " + mes + " de " + año + ", " + hora + ":" + minuto
				+ "hs";
		// Antiguo hardcode: composition.setTitle("Resumen del paciente al 18 de Febrero
		// de 2019, 14:00hs");
		composition.setTitle(actual);
		// Fin title

		composition.setStatus(Composition.CompositionStatus.fromCode("final"));
		composition.setDate(new Date());

		Reference ref = new Reference();
		composition.setSubject(ref.setReference(path+"Patient/"+identifier.getValue()));

		List<Reference> listRef = new ArrayList<>();
		Reference author = new Reference();
		author.setReference(path+"Device/1");
		listRef.add(author);

		composition.setAuthor(listRef);

		composition.setIdentifier(identifier);

		String profile = "http://hl7.org/fhir/uv/ips/StructureDefinition/composition-uv-ips";

		Meta meta1 = new Meta();
		meta1.addProfile(profile);

		composition.setMeta(meta1);

		Narrative narrative = new Narrative();
		narrative.setStatus(Narrative.NarrativeStatus.GENERATED);
		XhtmlNode xtmlnode1 = new XhtmlNode();
		xtmlnode1.setValue(
				"<div xmlns=\"http://www.w3.org/1999/xhtml\"><p><b>Generated Narrative with Details</b></p></div>");
		narrative.setDiv(xtmlnode1);

		composition.setText(narrative);

		composition.setCustodian(
				new Reference().setReference(path+"Organization/1"));

		List<Composition.CompositionAttesterComponent> listCompatt = new ArrayList<>();
		Composition.CompositionAttesterComponent attester = new Composition.CompositionAttesterComponent();
		attester.setMode(Composition.CompositionAttestationMode.LEGAL);
		attester.setTime(new Date());
		attester.setParty(
				new Reference().setReference(path+"Organization/1"));
		listCompatt.add(attester);

		composition.setAttester(listCompatt);
		// ******************

		GenerateHTML generateHTML = new GenerateHTML();

		List<Composition.SectionComponent> listaseccion = new ArrayList<>();
		// Section 0
		// parametros
		
		SectionComponent conditionSection = new SectionComponent();
		construirConditionSection(conditionSection);
		
		// ******************
		// Section 1
		// parametros
		String narrative_div1 = "<div xmlns=\"http://www.w3.org/1999/xhtml\">\n    <table>\n                                <thead>\n                                    <tr>\n                                        <th>Medicamento</th>\n                                        <th>Strength</th>\n                                        <th>Forma</th>\n                                        <th>Dosis</th>\n                                        <th>Comentario</th>\n                                    </tr>\n                                </thead>\n                                <tbody>\n                                    <tr>\n                                        <td>salbutamol (sustancia)</td>\n                                        <td>200 mcg</td>\n                                        <td>disparo</td>\n                                        <td>uno por día</td>\n                                        <td>tratamiento de asma</td>\n                                    </tr>\n                                                          </tbody>\n                            </table>\n                        </div>";

		try {
			narrative_div1 = generateHTML.generate(generateHTML.chargeTemplate("medication.ftl"), medication);
			System.out.println(narrative_div1);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		String codeable_code1 = "10160-0";
		String codeable_display1 = "Medication use";
		String codeable_system1 = "http://loinc.org";
		String lisref11 = path+"MedicationStatement/1";
		String sectitulo1 = "Medicamentos";
		Composition.SectionComponent seccion1 = construirSeccion0(narrative_div1, codeable_code1, codeable_display1,
				codeable_system1, lisref11, sectitulo1);
		// ******************
		// Section 2
		// parametros
		String narrative_div2 = "<div xmlns=\"http://www.w3.org/1999/xhtml\">Alergia a penicilina (trastorno), criticidad alta, activo</div>";

		try {
			narrative_div2 = generateHTML.generate(generateHTML.chargeTemplate("allergy.ftl"), allergyIntolerance);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		String codeable_code2 = "48765-2";
		String codeable_display2 = "Allergies and/or adverse reactions";
		String codeable_system2 = "http://loinc.org";
		String lisref12 = path+"AllergyIntolerance/1";
		String sectitulo2 = "Alergias";
		Composition.SectionComponent seccion2 = construirSeccion0(narrative_div2, codeable_code2, codeable_display2,
				codeable_system2, lisref12, sectitulo2);
		// ******************

		// Section 3
		// TODO: Revisar
		// parametros
		String narrative_div3 = "<div xmlns=\"http://www.w3.org/1999/xhtml\">\r\n"
				+ "                            <table>\r\n" + "                                <thead>\r\n"
				+ "                                    <tr>\r\n"
				+ "                                        <td>Descripción</td>\r\n"
				+ "                                        <td>Estado</td>\r\n"
				+ "                                        <td>Fecha</td>\r\n"
				+ "                                        <td>Protocolo/Serie</td>\r\n"
				+ "                                        <td>Organización</td>\r\n"
				+ "                                        <td>Ubicación</td>\r\n"
				+ "                                    </tr>\r\n" + "                                </thead>\r\n"
				+ "                                <tbody>\r\n" + "                                    <tr>\r\n"
				+ "                                        <td>Vacuna antigripal trivalente influenza A H1N1, H3 +\r\n"
				+ "                                            influenza B</td>\r\n"
				+ "                                        <td>Completo</td>\r\n"
				+ "                                        <td>2017</td>\r\n"
				+ "                                        <td>Antigripal Esquema Personal de Salud/1</td>\r\n"
				+ "                                        <td>Hospital Municipal Hospital Doctor Ángel Pintos</td>\r\n"
				+ "                                        <td>Hospital Municipal Hospital Doctor Ángel Pintos</td>\r\n"
				+ "                                    </tr>\r\n" + "                                </tbody>\r\n"
				+ "                            </table>\r\n" + "                        </div>";

		try {
			narrative_div3 = generateHTML.generate(generateHTML.chargeTemplate("immunization.ftl"), immunization);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		String codeable_code3 = "60484-3";
		String codeable_display3 = "Immunization record";
		String codeable_system3 = "http://loinc.org";
		String lisref3 = path+"Patient/"+identifier.getValue();
		String sectitulo3 = "Vacunas";
		Composition.SectionComponent seccion3 = construirSeccion0(narrative_div3, codeable_code3, codeable_display3,
				codeable_system3, lisref3, sectitulo3);

		// ******************

		listaseccion.add(conditionSection);
		listaseccion.add(seccion1);
		listaseccion.add(seccion2);
		listaseccion.add(seccion3);

		composition.setSection(listaseccion);

		return composition;
	}

	@Override
	public Object obtener() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void agregarEntries(List<BundleEntryComponent> entries) {
		Bundle.BundleEntryComponent entry00 = new Bundle.BundleEntryComponent();
		entry00.setFullUrl(path+"Composition/1");
		entry00.setResource(construir(identifier));
		entries.add(entry00);
	}

	public Composition.SectionComponent construirSeccion0(String narrative_div, String codeable_code,
			String codeable_display, String codeable_system, String lisref1, String sectitulo) {

		Composition.SectionComponent seccion = new Composition.SectionComponent();

		Narrative narrative1 = new Narrative();
		narrative1.setStatus(Narrative.NarrativeStatus.GENERATED);
		XhtmlNode xtmlnode2 = new XhtmlNode();
		xtmlnode2.setValue(narrative_div);
		narrative1.setDiv(xtmlnode2);

		CodeableConcept codeable1 = new CodeableConcept();
		Coding cod01 = new Coding();
		cod01.setCode(codeable_code);
		cod01.setDisplay(codeable_display);
		cod01.setSystem(codeable_system);
		codeable1.addCoding(cod01);

		List<Reference> listRefSection0 = new ArrayList<>();
		Reference refsection0 = new Reference();
		refsection0.setReference(lisref1);
		listRefSection0.add(refsection0);

		seccion.setText(narrative1);
		seccion.setCode(codeable1);
		seccion.setTitle(sectitulo);
		seccion.setEntry(listRefSection0);

		return seccion;

	}

	public void construirConditionSection(SectionComponent conditionSection) {
					
		Narrative text = new Narrative();
		text.setStatus(Narrative.NarrativeStatus.GENERATED);
	    XhtmlNode xtmlnode2=new XhtmlNode();
	    
	    GenerateHTML generateHTML = new GenerateHTML();		
	    String html = "";		    
	    try {
			html = generateHTML.generate(generateHTML.chargeTemplate("condition.ftl"), conditions);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}	
	    
	    xtmlnode2.setValue(html);
	    text.setDiv(xtmlnode2);
	    
	    conditionSection.setText(text);

	    CodeableConcept codeableConcept = new CodeableConcept();
	    Coding coding = new Coding();
	    coding.setCode("11450-4");
	    coding.setDisplay("Problem list");
	    coding.setSystem("http://loinc.org");
	    codeableConcept.addCoding(coding);

	    List<Reference> listaReference = new ArrayList<>();
	    	       
	    int i = 1;
	    for (Condition condition: conditions) {
	    	Reference reference = new Reference();
		    reference.setReference(path+"Condition/"+i);
		    listaReference.add(reference);
		    i++;
	    }
	    
//	    Reference reference = new Reference();
//	    reference.setReference(path+"Condition/1");
//	    listaReference.add(reference);
	    
	    conditionSection.setText(text);
	    conditionSection.setCode(codeableConcept);
	    conditionSection.setTitle("Problemas");
	    conditionSection.setEntry(listaReference);
	    		    
	}

}
