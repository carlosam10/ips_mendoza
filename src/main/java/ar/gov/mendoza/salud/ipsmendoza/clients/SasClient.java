package ar.gov.mendoza.salud.ipsmendoza.clients;
import ar.gov.mendoza.salud.ipsmendoza.sas.dao.local.ConditionLocal;
import ar.gov.mendoza.salud.ipsmendoza.sas.dao.local.Token;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

public class SasClient {

	private static final String REST_URI_TOKEN = "http://msaptst1.mendoza.gov.ar:8080/SamepWsTst/oauth/access_token";
	private static final String REST_URI_CONDI = "http://msaptst1.mendoza.gov.ar:8080/SamepWsTst/rest/wsproblemasxpaciente";
	private RestTemplate restTemplate;
	private static SasClient instance;	
	private Token token;
	
    private SasClient() {
    	restTemplate = new RestTemplate();
    	getToken();
    }
    
    public static SasClient getIntance() {
    	if(instance == null) {
    		instance = new SasClient();
    	}
    	return instance;
    }
    
    public void getToken() {
    	
    	//Se definen headers
    	HttpHeaders httpHeaders = new HttpHeaders();
    	//El tipo de contenido es texto plano
        httpHeaders.setContentType(MediaType.TEXT_PLAIN);
        //Se añaden las cookies
        httpHeaders.add("cookie", "JSESSIONID=84E39B345A8661BD6D3F0CF6B94FF974; JSESSIONID=84E39B345A8");
        
        String body = "text/plain\r\n" + 
        			  "client_id=b9e9cd02599c4e2aaf9a7ab40ef11139&granttype=password&scope=FullControl&username=wsalim&password=wssalud123";
        
        //Se asigna el body y los headers a la entidad
        HttpEntity<String> entity = new HttpEntity<String>(body, httpHeaders);
        
        //Se realiza la petición
        ResponseEntity<String> b = restTemplate.exchange(
        		REST_URI_TOKEN,
        		HttpMethod.POST,
        		entity,
        		String.class
        );
        
        Gson gson = new Gson();        
        this.token = gson.fromJson(b.getBody(), Token.class);
        
    }
    
    public List<ConditionLocal> getProblemas(String idProv) {
    	
    	//Pedir problemas
        HttpHeaders httpHeaders1 = new HttpHeaders();
        httpHeaders1.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders1.add("charset", "UTF-8");
        httpHeaders1.add("authorization", "OAuth "+this.token.getAccess_token());
        httpHeaders1.add("cookie", "JSESSIONID=84E39B345A8661BD6D3F0CF6B94FF974; JSESSIONID=84E39B345A8661BD6D3F0CF6B94FF974");
        
        Map<String, String> consulta = new HashMap<String, String>();
        consulta.put("idProv", idProv);
        
        Gson gson = new Gson();     
        String body = gson.toJson(consulta); 
        
        HttpEntity<String> entity1 = new HttpEntity<String>(body, httpHeaders1);
                
        ResponseEntity<String> result = new ResponseEntity<String>(HttpStatus.OK);
        
        try {
			
        	ResponseEntity<String> a = restTemplate.exchange(
            		REST_URI_CONDI,
            		HttpMethod.POST,
            		entity1,
            		String.class
            );
        	result = a;
        	
		} catch (HttpClientErrorException e) {
			
			System.out.println(e.getMessage());
			
			getToken();
        	getProblemas(idProv);
        	
        	
        	
		}
        
        //Se limpian los espacios innecesarios
        String cadenaLimpia = result.getBody();
        
        //Se obtiene el body del response y se lo transforma a objeto
        Object json = gson.fromJson(cadenaLimpia, Object.class); 
        
        //Se parsea ese objecto a un JSONElement 
        JsonElement jsonElement = gson.toJsonTree(json);
        
        //Se lo transforma a un JSONObject
        JsonObject jsonObject = (JsonObject) jsonElement;
        
        //Se extrae la propiedad wrapper "Problemas"
        jsonObject = (JsonObject) jsonObject.get("Problemas");      
        
        //Se obtiene el subarray "Problema" el cual posee todos los problemas
        JsonArray jsonArray = (JsonArray) jsonObject.get("Problema");
                
        //Se realiza la transformación de el JSONArray a un array simple de ConditionalLocal
        ConditionLocal [] data = gson.fromJson(jsonArray, ConditionLocal[].class);        
        
        //Se transforma el array simple a una List
        List<ConditionLocal> array = Arrays.asList(data);
        
        //Se devuelve la lista
        return array;
        
    }

}
