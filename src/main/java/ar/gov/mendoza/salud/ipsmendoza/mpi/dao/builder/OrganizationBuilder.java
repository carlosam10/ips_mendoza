package ar.gov.mendoza.salud.ipsmendoza.mpi.dao.builder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.hl7.fhir.r4.model.Bundle.BundleEntryComponent;
import org.hl7.fhir.r4.model.ContactPoint.ContactPointSystem;
import org.hl7.fhir.r4.model.ContactPoint.ContactPointUse;
import org.hl7.fhir.r4.model.Narrative.NarrativeStatus;
import org.hl7.fhir.utilities.xhtml.NodeType;
import org.hl7.fhir.utilities.xhtml.XhtmlNode;

import ar.gov.mendoza.salud.ipsmendoza.html.GenerateHTML;
import freemarker.template.TemplateException;

import org.hl7.fhir.r4.model.Address;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.ContactPoint;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Narrative;
import org.hl7.fhir.r4.model.Organization;
import org.hl7.fhir.r4.model.StringType;
import org.hl7.fhir.r4.model.Address.AddressUse;

public class OrganizationBuilder implements Builder<Organization, Object>{

	private Identifier identifier;
	
	public OrganizationBuilder(Identifier identifier) {

		this.identifier = identifier;
		
	}
	
	@Override
	public Organization construir(Identifier identifier) {
		Organization organization=new Organization();

        organization.setId("1");
        organization.setName("MINISTERIO DE SALUD-MENDOZA");
        organization.setActive(true);
        
        
        //Inicio Address
        
        //Lista de direcciones
        List<Address> addressList = new ArrayList<Address>();
        //Direccion simple a ser rellenada
        Address address = new Address();
        
        //Atributos simples
        address.setUse(AddressUse.WORK);
        address.setCountry("Argentina");
        address.setCity("Mendoza");
        address.setPostalCode("5500");
        
        //Lineas
        List<StringType> lines = new ArrayList<StringType>();
        StringType line = new StringType("Av. Peltier 351-5º Piso - Casa de Gobierno");
        lines.add(line);        
        address.setLine(lines);
        
        //Se agrega direccion a la lista de direcciones
        addressList.add(address);
        
        organization.setAddress(addressList);
        
        //Fin Address
        
        //Inicio Telecom
        
        //Lista de puntos de contactos
        List<ContactPoint> contactPoints = new ArrayList<ContactPoint>();
        //Punto de contacto
        ContactPoint contactPoint = new ContactPoint();
        
        contactPoint.setSystem(ContactPointSystem.PHONE);
        contactPoint.setValue("+54-0261-449-2958");
        contactPoint.setUse(ContactPointUse.WORK);
        
        //Se agrega contacto a la lista de contactos
        contactPoints.add(contactPoint);
        //Se agrega telecom al organization
        organization.setTelecom(contactPoints);        
        
        //Fin Telecom
        
        //Inicio Text
        GenerateHTML html = new GenerateHTML();
        
        Narrative narrative = new Narrative();

        String value = "MINISTERIO DE SALUD-MENDOZA";
//        
//		try {
//			value = html.generate(html.chargeTemplate("organization.ftl"), narrative);
//		} catch (IOException | TemplateException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
        
        narrative.setStatus(NarrativeStatus.GENERATED);
        
        XhtmlNode xhtmlNode = new XhtmlNode();
        xhtmlNode.setNodeType(NodeType.Text);
        xhtmlNode.setValue(value);   
        narrative.setDiv(xhtmlNode);
        
        organization.setText(narrative);
        
        //Fin Text
        
        //Inicio Identifier
        
        //Lista de identifiers
        List<Identifier> identifiers = new ArrayList<Identifier>();
        
        //Identificador del refes
        Identifier refes = new Identifier();
        refes.setSystem("https://bus.msal.gov.ar/dominios");
        refes.setValue("2.16.840.1.113883.2.10.28");
        identifiers.add(refes);
        
        //Identificador del dominio
        Identifier dominio = new Identifier();
        dominio.setSystem("http://argentina.gob.ar/salud/bus-interoperabilidad/dominio");
        dominio.setValue("http://salud.mendoza.gov.ar");
        identifiers.add(dominio);
        
        organization.setIdentifier(identifiers);
        
        //Fin Identifier
        
        return organization;
	}

	@Override
	public Object obtener() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void agregarEntries(List<BundleEntryComponent> entries) {
		Bundle.BundleEntryComponent entry = new Bundle.BundleEntryComponent();
        entry.setFullUrl("https://apisalud.mendoza.gov.ar/api/ips/fhir/Organization/1");
        entry.setResource(construir(identifier));
        entries.add(entry);
	}

}
