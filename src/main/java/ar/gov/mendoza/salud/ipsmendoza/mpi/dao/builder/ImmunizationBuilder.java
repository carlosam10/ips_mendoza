package ar.gov.mendoza.salud.ipsmendoza.mpi.dao.builder;

import java.util.ArrayList;
import java.util.List;

import org.hl7.fhir.r4.model.Bundle.BundleEntryComponent;
import org.hl7.fhir.r4.model.Immunization.ImmunizationStatus;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.CodeType;
import org.hl7.fhir.r4.model.CodeableConcept;
import org.hl7.fhir.r4.model.Coding;
import org.hl7.fhir.r4.model.DateTimeType;
import org.hl7.fhir.r4.model.Extension;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Immunization;
import org.hl7.fhir.r4.model.Reference;

public class ImmunizationBuilder implements Builder<Immunization, Object>{

	private Identifier identifier;
	
	public ImmunizationBuilder(Identifier identifier) {

		this.identifier = identifier;
		
	}
	
	@Override
	public Immunization construir(Identifier identifier) {
		//Recurso a devolver
    	Immunization immunization = new Immunization();
    	
    	immunization.setId("1");
    	
    	immunization.setStatus(ImmunizationStatus.COMPLETED);
    	
    	//Inicio VaccineCode
    	
    	CodeableConcept codeableVaccineCode = new CodeableConcept();
    	List<Coding> codingVaccines = new ArrayList<Coding>();
    	
    	Coding codingVaccine = new Coding();    
    	codingVaccine.setSystem("http://hl7.org/fhir/uv/ips/CodeSystem/absent-unknown-uv-ips");
    	codingVaccine.setCode("no-immunization-info");
    	codingVaccine.setDisplay("no-immunization-info");
    	codingVaccines.add(codingVaccine);
    	    	
    	codeableVaccineCode.setCoding(codingVaccines);
    	
    	immunization.setVaccineCode(codeableVaccineCode);
    	
    	//Fin VaccineCode
    	
    	//Inicio Patient
    	
    	Reference immunizationPatientReference = new Reference();
    	immunizationPatientReference.setReference("http://hapi.fhir.org/baseR4/Patient/1");
    	    	
    	immunization.setPatient(immunizationPatientReference);
    	
    	//Fin Patient
    	
    	//Inicio OcurrenceDateTime
    	
    	Extension extension = new Extension();
    	extension.setUrl("http://hl7.org/fhir/StructureDefinition/data-absent-reason");    	
    	CodeType valueCode = new CodeType();
    	valueCode.setValue("unknown");    	
    	extension.setValue(valueCode);
    	
    	DateTimeType dtType = new DateTimeType();
    	dtType.addExtension(extension);
    	
    	immunization.setOccurrence(dtType);
    	
    	//Fin OcurrenceDateTime    	
    	
    	return immunization;
	}

	@Override
	public Object obtener() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void agregarEntries(List<BundleEntryComponent> entries) {
		Bundle.BundleEntryComponent entry = new Bundle.BundleEntryComponent();
        entry.setFullUrl("https://apisalud.mendoza.gov.ar/api/ips/fhir/Immunization/1");
        entry.setResource(construir(identifier));
        entries.add(entry);	
	}

}
