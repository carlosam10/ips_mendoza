
package ar.gov.mendoza.salud.ipsmendoza.mpi.dao.local;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@XmlRootElement
@JsonIgnoreProperties(value = {"additionalProperties"}, ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class PatientLocal {

    private String resourceType;
    private String id;
    private Meta meta;
    private List<Extension> extension = new ArrayList<Extension>();
    private List<Identifier> identifier = new ArrayList<Identifier>();
    private Boolean active;
    private List<Name> name = new ArrayList<Name>();
    private String gender;
    private String birthDate;
    private List<Address> address = new ArrayList<Address>();
    private Long multipleBirthInteger;
    private List<Photo> photo = new ArrayList<Photo>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public PatientLocal() {
    }

    /**
     * 
     * @param extension
     * @param id
     * @param multipleBirthInteger
     * @param address
     * @param name
     * @param gender
     * @param active
     * @param birthDate
     * @param photo
     * @param identifier
     * @param meta
     * @param resourceType
     */
    public PatientLocal(String resourceType, String id, Meta meta, List<Extension> extension, List<Identifier> identifier, Boolean active, List<Name> name, String gender, String birthDate, List<Address> address, Long multipleBirthInteger, List<Photo> photo) {
        super();
        this.resourceType = resourceType;
        this.id = id;
        this.meta = meta;
        this.extension = extension;
        this.identifier = identifier;
        this.active = active;
        this.name = name;
        this.gender = gender;
        this.birthDate = birthDate;
        this.address = address;
        this.multipleBirthInteger = multipleBirthInteger;
        this.photo = photo;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public List<Extension> getExtension() {
        return extension;
    }

    public void setExtension(List<Extension> extension) {
        this.extension = extension;
    }

    public List<Identifier> getIdentifier() {
        return identifier;
    }

    public void setIdentifier(List<Identifier> identifier) {
        this.identifier = identifier;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public List<Name> getName() {
        return name;
    }

    public void setName(List<Name> name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public List<Address> getAddress() {
        return address;
    }

    public void setAddress(List<Address> address) {
        this.address = address;
    }

    public Long getMultipleBirthInteger() {
        return multipleBirthInteger;
    }

    public void setMultipleBirthInteger(Long multipleBirthInteger) {
        this.multipleBirthInteger = multipleBirthInteger;
    }

	public List<Photo> getPhoto() {
        return photo;
    }

    public void setPhoto(List<Photo> photo) {
        this.photo = photo;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
