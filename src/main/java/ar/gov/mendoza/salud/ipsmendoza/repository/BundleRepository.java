package ar.gov.mendoza.salud.ipsmendoza.repository;

import org.hl7.fhir.r4.model.Bundle;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by rajeevkumarsingh on 08/09/17.
 */
@Repository
public interface BundleRepository extends ReactiveMongoRepository<Bundle, String> {

}
