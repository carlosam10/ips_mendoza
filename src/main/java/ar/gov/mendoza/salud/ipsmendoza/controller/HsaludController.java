package ar.gov.mendoza.salud.ipsmendoza.controller;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.gclient.StringClientParam;
import ar.gov.mendoza.salud.ipsmendoza.clients.MPIClient;
import ar.gov.mendoza.salud.ipsmendoza.clients.SasClient;
import ar.gov.mendoza.salud.ipsmendoza.error.Error;
import ar.gov.mendoza.salud.ipsmendoza.html.GenerateHTML;
import ar.gov.mendoza.salud.ipsmendoza.mpi.dao.local.PatientLocal;
import ar.gov.mendoza.salud.ipsmendoza.mpi.dao.builder.AllergyIntoleranceBuilder;
import ar.gov.mendoza.salud.ipsmendoza.mpi.dao.builder.CompositionBuilder;
import ar.gov.mendoza.salud.ipsmendoza.mpi.dao.builder.ConditionBuilder;
import ar.gov.mendoza.salud.ipsmendoza.mpi.dao.builder.DeviceBuilder;
import ar.gov.mendoza.salud.ipsmendoza.mpi.dao.builder.ImmunizationBuilder;
import ar.gov.mendoza.salud.ipsmendoza.mpi.dao.builder.MedicationStatementBuilder;
import ar.gov.mendoza.salud.ipsmendoza.mpi.dao.builder.OrganizationBuilder;
import ar.gov.mendoza.salud.ipsmendoza.mpi.dao.builder.PatientBuilder;
import ar.gov.mendoza.salud.ipsmendoza.mpi.dao.local.Name;
import ar.gov.mendoza.salud.ipsmendoza.repository.BundleRepository;
import ar.gov.mendoza.salud.ipsmendoza.sas.dao.local.ConditionLocal;
import ar.gov.mendoza.salud.ipsmendoza.sas.dao.local.Token;

import org.checkerframework.checker.regex.RegexUtil;
import org.hl7.fhir.r4.formats.JsonCreator;
import org.hl7.fhir.r4.model.*;
import org.hl7.fhir.r4.model.Address.AddressUse;
import org.hl7.fhir.r4.model.AllergyIntolerance.AllergyIntoleranceCriticality;
import org.hl7.fhir.r4.model.AllergyIntolerance.AllergyIntoleranceReactionComponent;
import org.hl7.fhir.r4.model.AllergyIntolerance.AllergyIntoleranceSeverity;
import org.hl7.fhir.r4.model.AllergyIntolerance.AllergyIntoleranceType;
import org.hl7.fhir.r4.model.Bundle.BundleEntryComponent;
import org.hl7.fhir.r4.model.Bundle.BundleLinkComponent;
import org.hl7.fhir.r4.model.ContactPoint.ContactPointSystem;
import org.hl7.fhir.r4.model.ContactPoint.ContactPointUse;
import org.hl7.fhir.r4.model.Device.DeviceDeviceNameComponent;
import org.hl7.fhir.r4.model.Device.DeviceNameType;
import org.hl7.fhir.r4.model.DocumentReference.DocumentReferenceContentComponent;
import org.hl7.fhir.r4.model.DocumentReference.DocumentReferenceContextComponent;
import org.hl7.fhir.r4.model.Immunization.ImmunizationStatus;
import org.hl7.fhir.r4.model.Medication.MedicationStatus;
import org.hl7.fhir.r4.model.MedicationStatement.MedicationStatementStatus;
import org.hl7.fhir.r4.model.Narrative.NarrativeStatus;
import org.hl7.fhir.r4.model.OperationOutcome.IssueSeverity;
import org.hl7.fhir.r4.model.OperationOutcome.IssueType;
import org.hl7.fhir.r4.model.OperationOutcome.OperationOutcomeIssueComponent;
import org.hl7.fhir.r4.model.codesystems.DataAbsentReason;
import org.hl7.fhir.r4.model.codesystems.DocumentReferenceStatus;
import org.hl7.fhir.utilities.xhtml.NodeType;
import org.hl7.fhir.utilities.xhtml.XhtmlNode;
import org.json.JSONObject;
import org.json.XML;
import org.keycloak.authorization.client.util.Http;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sun.xml.bind.v2.runtime.MarshallerImpl;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.crypto.Data;
import javax.xml.namespace.QName;
import javax.xml.transform.stream.StreamSource;

/**
 * Created by Scaravelli on 21/05/19.
 */
@RestController
public class HsaludController {

    @Autowired

    private BundleRepository bundleRepository;

    private static final String REST_URI = "http://mpisaludprod.mendoza.gov.ar:8080/mpi-fhir/fhir/Patient/";
    private final String[] meses = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};
    
    @GetMapping(path = "/DocumentReference", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<String> getBundleDocumentReference(@RequestParam ("subject:identifier") String identifier, @RequestParam (name = "custodian", defaultValue = "") String custodian, @RequestParam (name = "class", defaultValue = "https://loinc.org/?s=60591-5") String classDef, @RequestParam(name = "_format", defaultValue = "json") String format) throws JsonProcessingException {
    System.out.println("Prueba DocumentReference");
    	FhirContext ctxR4 = FhirContext.forR4();
    	
    	String [] part = identifier.split("\\|");  	
    	
    	Bundle bundlePatient = new Bundle();
    	
    	try {
    		bundlePatient = getBundle(part[1]);
 		} catch (Exception e) {
 			OperationOutcome operationOutcome = Error.getOperationOutcome(IssueSeverity.ERROR, IssueType.PROCESSING, e.getMessage());
 			return Error.prettyReturn(operationOutcome, HttpStatus.SERVICE_UNAVAILABLE.value(), format);
 		} 
    	
        Bundle bundle=new Bundle();
        
        //Ver como definir id del bundle
        bundle.setId("1c89b971-1be2-4b03-960d-56836c711cff");
        bundle.setType(Bundle.BundleType.SEARCHSET);
        
        Meta meta = new Meta();
        meta.setLastUpdated(new Date());        
        bundle.setMeta(meta);
        
        //Modificar
        bundle.setTotal(1);
        		
        BundleLinkComponent bundleLinkComponent = new BundleLinkComponent();
        bundleLinkComponent.setRelation("self");
        
        if(custodian.equals("")) {
        	bundleLinkComponent.setUrl("https://apisalud.mendoza.gov.ar/api/ips/fhir/DocumentReference?subject:Patient.identifier="+identifier);
        }
        else {
        	bundleLinkComponent.setUrl("https://apisalud.mendoza.gov.ar/api/ips/fhir/DocumentReference?subject:Patient.identifier="+identifier+"&custodian="+custodian);
        }
        
        List<BundleLinkComponent> listOfBundleLinkComponent = new ArrayList<Bundle.BundleLinkComponent>();
        listOfBundleLinkComponent.add(bundleLinkComponent);
        
        bundle.setLink(listOfBundleLinkComponent);

        DocumentReference documentReference=new DocumentReference();

        List<Bundle.BundleEntryComponent> listaentry= new ArrayList();
        
        
        List<BundleEntryComponent> entries = new ArrayList<Bundle.BundleEntryComponent>();
        BundleEntryComponent entry = new BundleEntryComponent();
        
        
        
        //bundle.setEntry(theEntry)

        Bundle.BundleEntryComponent entry00 = new Bundle.BundleEntryComponent();
        entry00.setFullUrl("http://fhir.hl7fundamentals.org/r4/DocumentReference/"+part[1]);
                
        //Document Reference
        
        documentReference.setId(part[1]);
        meta.setLastUpdated(new Date());
        meta.setVersionId("1");
        documentReference.setMeta(meta);
        
        Identifier identifierDR = new Identifier();
        identifierDR.setSystem("https://apisalud.mendoza.gov.ar/api/ips/fhir/DocumentReference");
        identifierDR.setValue(part[1]);
        List<Identifier> identifiers = new ArrayList<Identifier>();
        identifiers.add(identifierDR);
        documentReference.setIdentifier(identifiers);
                
        CodeableConcept type = new CodeableConcept();
        Coding coding = new Coding();
        coding.setSystem("http://loinc.org");
        coding.setCode("60591-5");
        coding.setDisplay("Resumen de Historia Clínica");
        List<Coding> codings = new ArrayList<Coding>();
        codings.add(coding);
        type.setCoding(codings);
        documentReference.setType(type);
        
        //Subject
        
        Reference referenceSubject = new Reference();
        //referenceSubject.setDisplay(bundlePatient.get);
        String name = "";
        String birthdate = "";
        for (BundleEntryComponent be :bundlePatient.getEntry()) {
        	if(be.getResource().getResourceType().toString().equals("Patient")) {
        		for(HumanName humanName : ((Patient) be.getResource()).getName()) {
        			name = humanName.getText();
        		}
        		Date fecha = ((Patient) be.getResource()).getBirthDate();
        		
        		DateFormat formatoFecha = new SimpleDateFormat("dd-MM-yyyy");
        		birthdate = formatoFecha.format(fecha);
        	}
        }
        referenceSubject.setDisplay(name+" - "+birthdate);
        
        Identifier identifierSubject = new Identifier();
        identifierSubject.setSystem("http://apisalud.mendoza.gov.ar/api/ips/fhir/Patient");
        identifierSubject.setValue(part[1]);
        referenceSubject.setIdentifier(identifierSubject);
        documentReference.setSubject(referenceSubject);
        
        //Author
        
        List<Reference> references = new ArrayList<Reference>();
        
        Reference referenceAuthorDevice = new Reference();
        referenceAuthorDevice.setDisplay("Historia Clínica Electrónica");
        Identifier identifierAuthorDevice = new Identifier();
        identifierAuthorDevice.setSystem("http://apisalud.mendoza.gov.ar/api/ips/fhir/Device");
        identifierAuthorDevice.setValue("HCE");
        referenceAuthorDevice.setIdentifier(identifierAuthorDevice);
        references.add(referenceAuthorDevice);
        
        documentReference.setAuthor(references);
        
        // Text
        
        Narrative narrative = new Narrative();
        narrative.setStatus(NarrativeStatus.GENERATED);
        narrative.setDivAsString("<div xmlns=\"http://www.w3.org/1999/xhtml\">            <table>               <tr>                    <td>Ministerio de Salud Mendoza</td>                    <td>Resumen del paciente.</td>                </tr>            </table>            <a href=\"https://apisalud.mendoza.gov.ar/api/ips/fhir/Binary/"+part[1]+"\">Documento: Resumen HCE</a>        </div>");
        documentReference.setText(narrative);
        
        // MasterIdentifier
        
        documentReference.setMasterIdentifier(identifierDR);
        
        // Custodian
        
        Reference reference = new Reference();
        reference.setDisplay("MINISTERIO DE SALUD-MENDOZA");
        Identifier identifierReference = new Identifier();
        identifierReference.setSystem("https://bus.msal.gov.ar/dominios");
        identifierReference.setValue("2.16.840.1.113883.2.10.28");
        reference.setIdentifier(identifierReference);
        documentReference.setCustodian(reference);
        
        List<CodeableConcept> ccListSecurityLabel = new ArrayList<CodeableConcept>();
        CodeableConcept codeableConceptSecurityLabel = new CodeableConcept();
        List<Coding> cListSecurityLabel = new ArrayList<Coding>();
        Coding codingSecurityLabel = new Coding();
        codingSecurityLabel.setSystem("https://www.hl7.org/fhir/v3/Confidentiality/vs.html");
        codingSecurityLabel.setCode("N");
        codingSecurityLabel.setDisplay("Normal");
        cListSecurityLabel.add(codingSecurityLabel);
        codeableConceptSecurityLabel.setCoding(cListSecurityLabel);
        ccListSecurityLabel.add(codeableConceptSecurityLabel);
        documentReference.setSecurityLabel(ccListSecurityLabel);   
                                
        List<DocumentReferenceContentComponent> drccs = new ArrayList<DocumentReference.DocumentReferenceContentComponent>();
        DocumentReferenceContentComponent drcc = new DocumentReferenceContentComponent();
        Attachment attachment = new Attachment();
        attachment.setContentType("application/fhir+"+format.toLowerCase());
        attachment.setLanguage("es-AR");
        attachment.setUrl("https://apisalud.mendoza.gov.ar/api/ips/fhir/Binary/"+part[1]);
        attachment.setTitle("Resumen de HC");
        attachment.setCreation(new Date());
        drcc.setAttachment(attachment);
        documentReference.setContent(drccs);
        drccs.add(drcc);
                
        DocumentReferenceContentComponent drcc1 = new DocumentReferenceContentComponent();
        Coding coding2 = new Coding();
        coding2.setSystem("http://hl7.org/fhir/ValueSet/formatcodes");
        coding2.setCode("urn:ihe:iti:xds:2017:mimeTypeSufficient");
        coding2.setDisplay("mimeType Sufficient");
        drcc1.setFormat(coding2);
        drccs.add(drcc1);
        
        DocumentReferenceContextComponent drcc2 = new DocumentReferenceContextComponent();
        Period period = new Period();
        period.setStart(new Date());
        period.setEnd(new Date());
        drcc2.setPeriod(period);
        
        List<CodeableConcept> events = new ArrayList<CodeableConcept>();
        CodeableConcept cevent = new CodeableConcept();
        List<Coding> coevents = new ArrayList<Coding>();
        Coding coevent = new Coding();
        coevent.setSystem("apisalud.mendoza.gov.ar/api/ips/fhir/tiposevento");
        coevent.setCode("SIN_INFORMACION");
        coevents.add(coevent);
        events.add(cevent);
        cevent.setCoding(coevents);
        drcc2.setEvent(events);
        
        CodeableConcept cevent1 = new CodeableConcept();
        List<Coding> coevents1 = new ArrayList<Coding>();
        Coding coevent1 = new Coding();
        coevent1.setSystem("apisalud.mendoza.gov.ar/api/ips/fhir/tipoprestador");
        coevent1.setCode("SIN_INFORMACION");
        coevents1.add(coevent1);
        cevent1.setCoding(coevents1);
        drcc2.setFacilityType(cevent1);
        
        CodeableConcept cevent2 = new CodeableConcept();
        List<Coding> coevents2 = new ArrayList<Coding>();
        Coding coevent2 = new Coding();
        coevent2.setSystem("apisalud.mendoza.gov.ar/api/ips/fhir/especialidades");
        coevent2.setCode("SIN_INFORMACION");
        coevents2.add(coevent2);
        cevent2.setCoding(coevents2);
        drcc2.setPracticeSetting(cevent2);
        
        documentReference.setContext(drcc2);
        
        documentReference.setStatus(org.hl7.fhir.r4.model.Enumerations.DocumentReferenceStatus.CURRENT);
        
        //Fin Document Reference
        
        entry00.setResource(documentReference);
        listaentry.add(entry00);

        bundle.setEntry(listaentry);            
        
        if(format.equals("json")) {
        	String respuesta = ctxR4.newJsonParser().setPrettyPrint(true).encodeResourceToString(bundle);
        	return ResponseEntity.status(200).contentType(MediaType.valueOf("application/json;charset=UTF-8")).body(respuesta.toString());
        }
        else {
        	String respuesta = ctxR4.newXmlParser().setPrettyPrint(true).encodeResourceToString(bundle);
        	return ResponseEntity.status(200).contentType(MediaType.valueOf("application/xml;charset=UTF-8")).body(respuesta);
        }
        
    }

    @GetMapping(path = "/Binary/{id}", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<String> getBinary (@PathVariable(value = "id") String binaryId, @RequestParam(name = "_format", defaultValue = "json") String format) throws JsonProcessingException {

        //Bundle bundle=this.getIps(binaryId);
    	FhirContext ctxR4 = FhirContext.forR4();
        String bundle = "";

        try {
        	ResponseEntity<String> bundleResponse = this.getIps(binaryId, format);
        	try {
				if (bundleResponse.getStatusCode().equals(HttpStatus.SERVICE_UNAVAILABLE)) {
					throw new Exception("");
				}
				else {
					bundle = bundleResponse.getBody().toString();
				}
			} catch (Exception e) {
				return bundleResponse;
			}
		} catch (Exception e) {
			OperationOutcome operationOutcome = Error.getOperationOutcome(IssueSeverity.ERROR, IssueType.PROCESSING, e.getMessage());
			return Error.prettyReturn(operationOutcome, HttpStatus.SERVICE_UNAVAILABLE.value(), format);
		} 
        
         //String bundlestr = ctxR4.newJsonParser().encodeResourceToString(bundle);

        Binary binary = new Binary();
        binary.setContentType("application/"+format);
        ///Es el id provincial del MPI Mendoza??
        binary.setId(binaryId);

        String encodedString = Base64.getEncoder().encodeToString(bundle.getBytes());

        binary.setData(bundle.getBytes());
        
        if(format.equals("json")) {
        	String respuesta = ctxR4.newJsonParser().setPrettyPrint(true).encodeResourceToString(binary);
        	return ResponseEntity.status(200).contentType(MediaType.valueOf("application/json;charset=UTF-8")).body(respuesta.toString());
        }
        else {
        	String respuesta = ctxR4.newXmlParser().setPrettyPrint(true).encodeResourceToString(binary);
        	return ResponseEntity.status(200).contentType(MediaType.valueOf("application/xml;charset=UTF-8")).body(respuesta);
        }
        
    }

    @GetMapping(path = "/Bundle/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getIps (@PathVariable(value = "id") String ipsId, @RequestParam(name = "_format", defaultValue = "json") String format) throws JsonProcessingException {
    	
    	Bundle bundle = new Bundle();
    	
    	try {
    		 bundle = getBundle(ipsId);
		} catch (Exception e) {
			OperationOutcome operationOutcome = Error.getOperationOutcome(IssueSeverity.ERROR, IssueType.PROCESSING, e.getMessage());
			return Error.prettyReturn(operationOutcome, HttpStatus.SERVICE_UNAVAILABLE.value(), format);
		}

        FhirContext ctxR4 = FhirContext.forR4();
    	
        //String respuesta = ctxR4.newXmlParser().setPrettyPrint(true).encodeResourceToString(bundle);        
    	//return ResponseEntity.status(200).contentType(MediaType.valueOf("application/xml;charset=UTF-8")).body(respuesta);
        		
    	if(format.equals("json")) {
        	String respuesta = ctxR4.newJsonParser().setPrettyPrint(true).encodeResourceToString(bundle);
        	return ResponseEntity.status(200).contentType(MediaType.valueOf("application/json;charset=UTF-8")).body(respuesta.toString());
        }
        else {
        	String respuesta = ctxR4.newXmlParser().setPrettyPrint(true).encodeResourceToString(bundle);
        	return ResponseEntity.status(200).contentType(MediaType.valueOf("application/xml;charset=UTF-8")).body(respuesta);
        }
    	
    }    
        
    public Bundle getBundle(String ipsId) throws Exception {
    	
    	Bundle bundle=new Bundle();
        bundle.setId("Resumen-HC-IPS-SaludDigital.ar");

        Meta meta=new Meta();
        meta.setLastUpdated(new Date());
        bundle.setLanguage("es-AR");

        bundle.setMeta(meta);
        bundle.setType(Bundle.BundleType.DOCUMENT);

        Identifier identifier= new Identifier();
        //identifier.setSystem("2.16.840.1.113883.2.10.28"); /*oid mendoza ?*/
        identifier.setSystem("http://salud.mendoza.gov.ar");
        identifier.setValue(ipsId);

        bundle.setIdentifier(identifier);
        
        try {
			
        	PatientBuilder patientBuilder = new PatientBuilder(identifier);	 
            OrganizationBuilder organizationBuilder = new OrganizationBuilder(identifier);  
            ConditionBuilder conditionBuilder = new ConditionBuilder(identifier);   
            ImmunizationBuilder immunizationBuilder = new ImmunizationBuilder(identifier);        
            AllergyIntoleranceBuilder allergyIntoleranceBuilder = new AllergyIntoleranceBuilder();        
            MedicationStatementBuilder medicationStatementBuilder = new MedicationStatementBuilder(identifier);
            DeviceBuilder deviceBuilder = new DeviceBuilder(identifier);        
            List<Bundle.BundleEntryComponent> listaentry= new ArrayList();
            
            //Ultimo en generarse ya que este posee las referencias a todo lo demas        
            CompositionBuilder compositionBuilder = new CompositionBuilder(identifier, immunizationBuilder.construir(identifier), allergyIntoleranceBuilder.construir(identifier), medicationStatementBuilder.construir(identifier), conditionBuilder.getConditions());
            compositionBuilder.agregarEntries(listaentry);
            
            try {
            	patientBuilder.agregarEntries(listaentry);
    		} catch (Exception e) {
    			throw new Exception(e.getMessage());
    		}
            
            
            organizationBuilder.agregarEntries(listaentry);        
            immunizationBuilder.agregarEntries(listaentry);
            medicationStatementBuilder.agregarEntries(listaentry);
            allergyIntoleranceBuilder.agregarEntries(listaentry);
            deviceBuilder.agregarEntries(listaentry);
            conditionBuilder.agregarEntries(listaentry);
            
            bundle.setEntry(listaentry);
        	
        	return bundle;
        	
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
        
	    
    }
           
    //Ejemplo de uso de FHIR
    @GetMapping("/Patient/{id}")
    public List<ConditionLocal> getPatient (@PathVariable(value = "id") String patientId) {
    	
    	
    	//CODIGO DE PRUEBA
    	
    	SasClient client = SasClient.getIntance();
    	
    	return client.getProblemas(patientId);
    	
    	//CODIGO DE PRUEBA

//    	Identifier identifier= new Identifier();
//        //identifier.setSystem("2.16.840.1.113883.2.10.28"); /*oid mendoza ?*/
//        identifier.setSystem("http://salud.mendoza.gov.ar");
//        identifier.setValue(patientId);
//    	
//        PatientBuilder patientBuilder = new PatientBuilder(identifier);        
//    	Patient patient = patientBuilder.construir(identifier);
//    	
//    	
//        // ...populate...
//        //patient.addIdentifier().setSystem("urn:mrns").setValue("12345");
//        //patient.addName().setFamily("Smith").addGiven("Tester").addGiven("Q");
//
//        FhirContext ctxR4 = FhirContext.forR4();
//        String resp = ctxR4.newJsonParser().setPrettyPrint(true).encodeResourceToString(patient);

        //return resp;
        
    }

}
