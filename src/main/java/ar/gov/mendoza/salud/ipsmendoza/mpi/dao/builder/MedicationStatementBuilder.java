package ar.gov.mendoza.salud.ipsmendoza.mpi.dao.builder;

import java.util.ArrayList;
import java.util.List;

import org.hl7.fhir.r4.model.Bundle.BundleEntryComponent;
import org.hl7.fhir.r4.model.MedicationStatement.MedicationStatementStatus;
import org.hl7.fhir.r4.model.Narrative.NarrativeStatus;
import org.hl7.fhir.utilities.xhtml.NodeType;
import org.hl7.fhir.utilities.xhtml.XhtmlNode;

import ar.gov.mendoza.salud.ipsmendoza.html.GenerateHTML;

import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.CanonicalType;
import org.hl7.fhir.r4.model.CodeType;
import org.hl7.fhir.r4.model.CodeableConcept;
import org.hl7.fhir.r4.model.Coding;
import org.hl7.fhir.r4.model.Extension;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.MedicationStatement;
import org.hl7.fhir.r4.model.Meta;
import org.hl7.fhir.r4.model.Narrative;
import org.hl7.fhir.r4.model.Period;
import org.hl7.fhir.r4.model.Reference;

public class MedicationStatementBuilder implements Builder<MedicationStatement, Object>{

	private Identifier identifier;
	
	public MedicationStatementBuilder(Identifier identifier) {

		this.identifier = identifier;
		
	}
	
	@Override
	public MedicationStatement construir(Identifier identifier) {
		MedicationStatement medication = new MedicationStatement();
		
        medication.setId("1");
        
        medication.setStatus(MedicationStatementStatus.ACTIVE);
        
        //Inicio MedicationCodeableConcept
        
        CodeableConcept codeableMedication = new CodeableConcept();        
        List<Coding> codingMedications = new ArrayList<Coding>();
        
        Coding codingMeditacion = new Coding();
        codingMeditacion.setSystem("http://hl7.org/fhir/uv/ips/CodeSystem/absent-unknown-uv-ips");
        codingMeditacion.setCode("no-medication-info");
        codingMeditacion.setDisplay("No information about current medications");
        codingMedications.add(codingMeditacion);
        
        codeableMedication.setCoding(codingMedications);
        
        medication.setMedication(codeableMedication);
                
        //Fin MedicationCodeableConcept
        
        //Inicio EffectivePeriod
        
        Period period = new Period();
        
        Extension medicationExtension = new Extension();
        medicationExtension.setUrl("http://hl7.org/fhir/StructureDefinition/data-absent-reason");        
        CodeType valueCode = new CodeType();        
    	valueCode.setValue("unknown");    	    	
        medicationExtension.setValue(valueCode);
        period.addExtension(medicationExtension);
        
        medication.setEffective(period);
        
        //Fin EffectivePeriod
        
        //Inicio Subject
        
        Reference reference = new Reference();
        reference.setReference("https://apisalud.mendoza.gov.ar/api/ips/fhir/Patient/"+identifier.getValue());
        medication.setSubject(reference);
        
        //Fin Subject
        
        //Inicio Meta
        
        Meta meta = new Meta();
        List<CanonicalType> profiles = new ArrayList<CanonicalType>();
        CanonicalType profile = new CanonicalType();
        
        profile.setValue("http://hl7.org/fhir/uv/ips/StructureDefinition/medicationstatement-uv-ips");
        profiles.add(profile);
        
        meta.setProfile(profiles);
        medication.setMeta(meta);
        
        //Fin Meta
        
        //Inicio Text        
        
        Narrative narrative = new Narrative();
        narrative.setStatus(NarrativeStatus.GENERATED);
        
        XhtmlNode xhtmlNode = new XhtmlNode();
        xhtmlNode.setNodeType(NodeType.Text);
        
        GenerateHTML generateHTML = new GenerateHTML();    
        
        try {
        	xhtmlNode.setValue(generateHTML.generate(generateHTML.chargeTemplate("medication.ftl"), medication)); 
		} catch (Exception e) {
			System.out.println("Error al generar el HTML de Medication");
		}        
          
        narrative.setDiv(xhtmlNode);
        
        medication.setText(narrative);
        
        //Fin Text
        

        return medication;
	}

	@Override
	public Object obtener() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void agregarEntries(List<BundleEntryComponent> entries) {
		Bundle.BundleEntryComponent entry = new Bundle.BundleEntryComponent();
        entry.setFullUrl("https://apisalud.mendoza.gov.ar/api/ips/fhir/MedicationStatement/1");
        entry.setResource(construir(identifier));
        entries.add(entry);
	}

}
