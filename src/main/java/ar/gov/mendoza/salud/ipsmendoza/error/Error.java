package ar.gov.mendoza.salud.ipsmendoza.error;

import java.util.ArrayList;
import java.util.List;

import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.OperationOutcome;
import org.hl7.fhir.r4.model.OperationOutcome.IssueSeverity;
import org.hl7.fhir.r4.model.OperationOutcome.IssueType;
import org.hl7.fhir.r4.model.OperationOutcome.OperationOutcomeIssueComponent;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.core.JsonProcessingException;

import ca.uhn.fhir.context.FhirContext;

public class Error {

	public static OperationOutcome getOperationOutcome(IssueSeverity issueSeverity, IssueType issueType, String diagnostics) throws JsonProcessingException {
		
		OperationOutcome operationOutcome = new OperationOutcome();			
		List<OperationOutcomeIssueComponent> issues = new ArrayList<OperationOutcome.OperationOutcomeIssueComponent>();			
		OperationOutcomeIssueComponent operationOutcomeIssueComponent = new OperationOutcomeIssueComponent();
		operationOutcomeIssueComponent.setSeverity(issueSeverity);
		operationOutcomeIssueComponent.setCode(issueType);
		operationOutcomeIssueComponent.setDiagnostics(diagnostics);
		issues.add(operationOutcomeIssueComponent);			
		operationOutcome.setIssue(issues);
		
		return operationOutcome;
	}

	public static ResponseEntity<String> prettyReturn(IBaseResource fhir, int status, String format) {
		
		FhirContext ctxR4 = FhirContext.forR4();
		String respuesta = "";		
		
		if (format.equals("xml")) {
			respuesta = ctxR4.newXmlParser().setPrettyPrint(true).encodeResourceToString(fhir);	
			return ResponseEntity.status(status).contentType(MediaType.valueOf("application/"+format+";charset=UTF-8")).body(respuesta.toString());
		}
		else {
			respuesta = ctxR4.newJsonParser().setPrettyPrint(true).encodeResourceToString(fhir);
			return ResponseEntity.status(status).contentType(MediaType.valueOf("application/json;charset=UTF-8")).body(respuesta.toString());
		}
	}
	
}
