
package ar.gov.mendoza.salud.ipsmendoza.mpi.dao.local;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@JsonIgnoreProperties({"additionalProperties"})
public class Name {

    private String use;
    private String text;
    private List<String> family = new ArrayList<String>();
   // @JsonProperty("_family") private EFamily eFamily;
    private List<String> given = new ArrayList<String>();
  //  private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Name() {
    }

    /**
     * 
     * @param given
     * @param family
     * @param use
     */
    public Name(String use, String text, List<String> family, List<String> given) {
        //, EFamily eFamily
        super();
        this.use = use;
        this.text = text;
        this.family = family;
       // this.eFamily = eFamily;
        this.given = given;
    }

    public String getUse() {
        return use;
    }

    public void setUse(String use) {
        this.use = use;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<String> getFamily() {
        return family;
    }

    public void setFamily(List<String> family) {
        this.family = family;
    }

    public List<String> getGiven() {
        return given;
    }

    public void setGiven(List<String> given) {
        this.given = given;
    }

}
