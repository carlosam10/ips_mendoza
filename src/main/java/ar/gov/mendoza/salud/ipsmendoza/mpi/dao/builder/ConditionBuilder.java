package ar.gov.mendoza.salud.ipsmendoza.mpi.dao.builder;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hl7.fhir.r4.model.Bundle.BundleEntryComponent;
import org.hl7.fhir.r4.model.Narrative.NarrativeStatus;
import org.hl7.fhir.utilities.xhtml.XhtmlNode;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.CodeType;
import org.hl7.fhir.r4.model.CodeableConcept;
import org.hl7.fhir.r4.model.Coding;
import org.hl7.fhir.r4.model.Condition;
import org.hl7.fhir.r4.model.DateTimeType;
import org.hl7.fhir.r4.model.Extension;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Narrative;
import org.hl7.fhir.r4.model.Reference;

import ar.gov.mendoza.salud.ipsmendoza.clients.SasClient;
import ar.gov.mendoza.salud.ipsmendoza.html.GenerateHTML;
import ar.gov.mendoza.salud.ipsmendoza.sas.dao.local.ConditionLocal;

public class ConditionBuilder implements Builder<Condition, Object> {

	private Identifier identifier;

	SasClient sasClient;
	
	private List<Condition> conditions;

	public ConditionBuilder(Identifier identifier) throws Exception {
		this.identifier = identifier;
		conditions = new ArrayList<Condition>();
		try {
			sasClient = SasClient.getIntance();
			transformar();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new Exception("No se puede acceder a los datos del servidor. Problema de Acceso a recurso condition.");
		}
	}

	public Condition construir(ConditionLocal c) {

		Condition condition = c.obtener();

		condition.setId("IPS-examples-Condition-01");

		// Inicio Text

		//TODO: BUSCAR PLANTILLA CONDITION INDIVIDUAL
		Narrative narrative = new Narrative();
		narrative.setStatus(NarrativeStatus.EMPTY);
		
//		GenerateHTML generateHTML = new GenerateHTML();		
//	    String html = "";		    
//	    try {
//			html = generateHTML.generate(generateHTML.chargeTemplate("condition.ftl"), conditions);
//		} catch (Exception e) {
//			System.out.println(e.getMessage());
//		}	
//	    
//	    XhtmlNode xhtmlnode = new XhtmlNode();
//	    xhtmlnode.setValue(html);
//	    narrative.setDiv(xhtmlnode);
	    
		condition.setText(narrative);

		// Fin Text

		// Inicio ClinicalStatus

		CodeableConcept codeableClinicalStatus = new CodeableConcept();
		Coding codeClinicalStatus = new Coding();
		codeClinicalStatus.setSystem("http://terminology.hl7.org/CodeSystem/condition-clinical");
		codeClinicalStatus.setCode("active");
		codeableClinicalStatus.addCoding(codeClinicalStatus);

		condition.setClinicalStatus(codeableClinicalStatus);

		// Final ClinicalStatus

		// Inicio VerificationStatus

		CodeableConcept codeableVerificationStatus = new CodeableConcept();
		Coding codeVerificationStatus = new Coding();
		codeVerificationStatus.setSystem("http://terminology.hl7.org/CodeSystem/condition-ver-status");
		codeVerificationStatus.setCode("confirmed");
		codeableVerificationStatus.addCoding(codeVerificationStatus);

		condition.setVerificationStatus(codeableVerificationStatus);

		// Fin VerificationStatus

		// Inicio Category

		List<CodeableConcept> conditionCategories = new ArrayList<CodeableConcept>();
		CodeableConcept conditionCategory = new CodeableConcept();

		List<Coding> codeCategories = new ArrayList<Coding>();
		Coding codeCategory = new Coding();
		codeCategory.setSystem("http://loinc.org");
		codeCategory.setCode("75326-9");
		codeCategory.setDisplay("Problem");
		codeCategories.add(codeCategory);

		conditionCategories.add(conditionCategory);

		conditionCategory.setCoding(codeCategories);

		condition.setCategory(conditionCategories);

		// Fin Category

		// Inicio Code

		CodeableConcept codeableCondition = new CodeableConcept();

		List<Coding> codingConditions = new ArrayList<Coding>();
		Coding codingCondition = new Coding();
		codingCondition.setSystem("http://snomed.info/sct");
		codingCondition.setCode(c.getCodigoSNOMED().trim());
		codingCondition.setDisplay(c.getDescSNOMED().trim());
		codingConditions.add(codingCondition);

		codeableCondition.setCoding(codingConditions);

		condition.setCode(codeableCondition);

		// Fin Code

		// Inicio Subject

		Reference referenceCondition = new Reference();
		referenceCondition.setReference("http://fhir.msal.gov.ar/Patient/1");
		condition.setSubject(referenceCondition);

		// Fin Subject

		// Inicio onsetDateTime

		// TODO: Averiguar acerca de la construcción del onsetTime
		// condition.setOnset(value)

//		List<Extension> extensionConditions = new ArrayList<Extension>();
//		Extension extensionCondition = new Extension();
//		extensionCondition.setUrl("http://hl7.org/fhir/StructureDefinition/data-absent-reason");
//
//		CodeType codeCondition = new CodeType();
//		codeCondition.setValue("unknown");
//		extensionCondition.setValue(codeCondition);
//		extensionConditions.add(extensionCondition);
//
//		DateTimeType conditionOnSet = new DateTimeType();
//		conditionOnSet.setExtension(extensionConditions);
//
//		condition.setOnset(conditionOnSet);
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		
		LocalDate localDate = LocalDate.parse(c.getFecha(), formatter);
		Date date = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
		
		DateTimeType dateTimeType = new DateTimeType();
		dateTimeType.setValue(date);
		condition.setOnset(dateTimeType);
		condition.setRecordedDate(date);

		// Fin onsetDateTime

		return condition;

	}

	@Override
	public List<ConditionLocal> obtener() throws Exception {
		try {
			List<ConditionLocal> lista = sasClient.getProblemas(identifier.getValue());
			return lista;
		} catch (Exception e) {
			throw new Exception("No se puede acceder a los datos del servidor. Problema de Acceso a recurso condition.");
		}		
	}
	
	public void transformar() throws Exception{
		try {
			List<ConditionLocal> lista = this.obtener();
			if (lista.size() > 0) {
				for (ConditionLocal c : lista) {
					this.conditions.add(construir(c));
				}
			} else {
				this.conditions.add(defaultCondition());
			}
		} catch (Exception e) {
			throw new Exception("No se puede acceder a los datos del servidor. Problema de Acceso a recurso condition.");
		}		
	}

	@Override
	public void agregarEntries(List<BundleEntryComponent> entries) {
		int i = 1;
		for (Condition item : conditions) {
			item.setId(i+"");
			Bundle.BundleEntryComponent entry = new Bundle.BundleEntryComponent();
			entry.setFullUrl("https://apisalud.mendoza.gov.ar/api/ips/fhir/Condition/"+i);
			entry.setResource(item);
			entries.add(entry);
			i++;
		}
	}

	@Override
	public Condition construir(Identifier identifier) {
		// TODO Auto-generated method stub
		return null;
	}

	public Condition defaultCondition() {
		Condition condition = new Condition();

		condition.setId("1");

		// Inicio Text

		Narrative narrative = new Narrative();
		narrative.setStatus(NarrativeStatus.EMPTY);

		condition.setText(narrative);

		// Fin Text

		// Inicio ClinicalStatus

		CodeableConcept codeableClinicalStatus = new CodeableConcept();
		Coding codeClinicalStatus = new Coding();
		codeClinicalStatus.setSystem("http://terminology.hl7.org/CodeSystem/condition-clinical");
		codeClinicalStatus.setCode("active");
		codeableClinicalStatus.addCoding(codeClinicalStatus);

		condition.setClinicalStatus(codeableClinicalStatus);

		// Final ClinicalStatus

		// Inicio VerificationStatus

		CodeableConcept codeableVerificationStatus = new CodeableConcept();
		Coding codeVerificationStatus = new Coding();
		codeVerificationStatus.setSystem("http://terminology.hl7.org/CodeSystem/condition-ver-status");
		codeVerificationStatus.setCode("confirmed");
		codeableVerificationStatus.addCoding(codeVerificationStatus);

		condition.setVerificationStatus(codeableVerificationStatus);

		// Fin VerificationStatus

		// Inicio Category

		List<CodeableConcept> conditionCategories = new ArrayList<CodeableConcept>();
		CodeableConcept conditionCategory = new CodeableConcept();

		List<Coding> codeCategories = new ArrayList<Coding>();
		Coding codeCategory = new Coding();
		codeCategory.setSystem("http://loinc.org");
		codeCategory.setCode("75326-9");
		codeCategory.setDisplay("Problem");
		codeCategories.add(codeCategory);

		conditionCategories.add(conditionCategory);

		conditionCategory.setCoding(codeCategories);

		condition.setCategory(conditionCategories);

		// Fin Category

		// Inicio Code

		CodeableConcept codeableCondition = new CodeableConcept();

		List<Coding> codingConditions = new ArrayList<Coding>();
		Coding codingCondition = new Coding();
		codingCondition.setSystem("http://hl7.org/fhir/uv/ips/CodeSystem/absent-unknown-uv-ips");
		codingCondition.setCode("no-problem-info");
		codingCondition.setDisplay("No known problems");
		codingConditions.add(codingCondition);

		codeableCondition.setCoding(codingConditions);

		condition.setCode(codeableCondition);

		// Fin Code

		// Inicio Subject

		Reference referenceCondition = new Reference();
		referenceCondition.setReference("http://fhir.msal.gov.ar/Patient/1");
		condition.setSubject(referenceCondition);

		// Fin Subject

		// Inicio onsetDateTime

		// TODO: Averiguar acerca de la construcción del onsetTime
		// condition.setOnset(value)

		List<Extension> extensionConditions = new ArrayList<Extension>();
		Extension extensionCondition = new Extension();
		extensionCondition.setUrl("http://hl7.org/fhir/StructureDefinition/data-absent-reason");

		CodeType codeCondition = new CodeType();
		codeCondition.setValue("unknown");
		extensionCondition.setValue(codeCondition);
		extensionConditions.add(extensionCondition);

		DateTimeType conditionOnSet = new DateTimeType();
		conditionOnSet.setExtension(extensionConditions);

		condition.setOnset(conditionOnSet);

		// Fin onsetDateTime

		return condition;
	}

	public List<Condition> getConditions() {
		return conditions;
	}

}
