package ar.gov.mendoza.salud.ipsmendoza.sas.dao.local;

import java.util.ArrayList;
import java.util.List;

import org.hl7.fhir.r4.model.CodeableConcept;
import org.hl7.fhir.r4.model.Coding;
import org.hl7.fhir.r4.model.Condition;
import org.springframework.format.datetime.standard.DateTimeFormatterFactoryBean;

import com.google.gson.annotations.SerializedName;

import ar.gov.mendoza.salud.ipsmendoza.mpi.dao.local.Identifier;
import ar.gov.mendoza.salud.ipsmendoza.mpi.dao.local.Meta;

public class ConditionLocal {

	@SerializedName("CUILMedico")
	private String cuilMedico;

	@SerializedName("CodigoSNOMED")
	private String codigoSNOMED;

	@SerializedName("DescSNOMED")
	private String descSNOMED;

	@SerializedName("Fecha")
	private String fecha;

	public ConditionLocal() {
	}

	public String getCuilMedico() {
		return cuilMedico;
	}

	public void setCuilMedico(String cuilMedico) {
		this.cuilMedico = cuilMedico.trim();
	}

	public String getCodigoSNOMED() {
		return codigoSNOMED;
	}

	public void setCodigoSNOMED(String codigoSNOMED) {
		this.codigoSNOMED = codigoSNOMED.trim();
	}

	public String getDescSNOMED() {
		return descSNOMED;
	}

	public void setDescSNOMED(String descSNOMED) {
		this.descSNOMED = descSNOMED.trim();
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha.trim();
	}

	public Condition obtener() {

		Condition condition = new Condition();

		CodeableConcept codeableCondition = new CodeableConcept();

		List<Coding> codingConditions = new ArrayList<Coding>();
		Coding codingCondition = new Coding();
		codingCondition.setSystem("http://snomed.info/sct");
		codingCondition.setCode(codigoSNOMED);
		codingCondition.setDisplay(descSNOMED);
		codingConditions.add(codingCondition);

		codeableCondition.setCoding(codingConditions);

		condition.setCode(codeableCondition);

		return condition;
	}

}
