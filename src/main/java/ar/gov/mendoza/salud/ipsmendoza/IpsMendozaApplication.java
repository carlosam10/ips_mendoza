package ar.gov.mendoza.salud.ipsmendoza;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

@SpringBootApplication
@EnableReactiveMongoRepositories
public class IpsMendozaApplication {

	public static void main(String[] args) {
		SpringApplication.run(IpsMendozaApplication.class, args);
	}
}
