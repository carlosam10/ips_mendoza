package ar.gov.mendoza.salud.ipsmendoza.mpi.dao.builder;

import java.util.ArrayList;
import java.util.List;

import org.hl7.fhir.r4.model.Bundle.BundleEntryComponent;
import org.hl7.fhir.r4.model.Device.DeviceDeviceNameComponent;
import org.hl7.fhir.r4.model.Device.DeviceNameType;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.CodeableConcept;
import org.hl7.fhir.r4.model.Coding;
import org.hl7.fhir.r4.model.Device;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Reference;

public class DeviceBuilder implements Builder<Device, Object>{

	private Identifier identifier;
	
	public DeviceBuilder(Identifier identifier) {

		this.identifier = identifier;
		
	}
	
	@Override
	public Device construir(Identifier identifier) {
		Device device = new Device();
    	
    	device.setId("1");    	
    	
    	//Inicio Identificadores 
    	List<Identifier> identifiers = new ArrayList<Identifier>();
    	
    	//TODO: Verificar origen del identifier
    	
    	//for para recorrer diferentes identificadores
    	Identifier identifierDevice1 = new Identifier();
    	identifierDevice1.setSystem("https://apisalud.mendoza.gov.ar/api/ips/fhir/Device");
    	identifierDevice1.setValue("HCE");
    	identifiers.add(identifierDevice1);
    	//fin bucle for
    	
    	device.setIdentifier(identifiers);
    	//Fin Identificadores 
    	
    	//Inicio deviceName  
    	
    	//for para recorrer diferentes deviceName
    	DeviceDeviceNameComponent ddNameComponent = new DeviceDeviceNameComponent();
    	ddNameComponent.setName("Sistema HCE Mendoza");
    	ddNameComponent.setType(DeviceNameType.MANUFACTURERNAME);
    	device.addDeviceName(ddNameComponent);
    	//fin bucle for
    	
    	//Fin deviceName
    	
    	//Inicio Type
    	
    	CodeableConcept codeableConcept = new CodeableConcept();
    	
    	//for para recorrer todos los codings
    	Coding coding = new Coding();
    	coding.setSystem("http://snomed.info/sct");
    	coding.setCode("462894001");
    	coding.setDisplay("software de aplicación de sistema de información de historias clínicas de pacientes (objeto físico)");
    	codeableConcept.addCoding(coding);
    	//fin bucle for
    	
    	device.setType(codeableConcept);
    	
    	//Fin type
    	
    	//Inicio owner
    	
    	Reference reference = new Reference();
    	//TODO: Revisar ya que difiere el JSON provisto por Nación de la documentación de SIMPLIFIER.NET
    	reference.setReference("https://apisalud.mendoza.gov.ar/api/ips/fhir/Organization/1");
    	device.setOwner(reference);
    	
    	//Fin owner
    	
    	return device;
	}

	@Override
	public Object obtener() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void agregarEntries(List<BundleEntryComponent> entries) {
		Bundle.BundleEntryComponent entry = new Bundle.BundleEntryComponent();
        entry.setFullUrl("https://apisalud.mendoza.gov.ar/api/ips/fhir/Device/1");
        entry.setResource(construir(identifier));
        entries.add(entry);
	}

}
