package ar.gov.mendoza.salud.ipsmendoza.html;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.MedicationStatement;
import org.hl7.fhir.r4.model.Organization;
import org.hl7.fhir.r4.model.Patient;

import ar.gov.mendoza.salud.ipsmendoza.DemofreemakerApplication;
import ar.gov.mendoza.salud.ipsmendoza.ValueExampleObject;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.Version;

public class GenerateHTML {

	public Template chargeTemplate(String templateName) throws IOException{
		Configuration cfg = new Configuration(new Version("2.3.23"));

        cfg.setClassForTemplateLoading(DemofreemakerApplication.class, "/");
        cfg.setDefaultEncoding("UTF-8");

        Template template = null;
        try {
            return cfg.getTemplate(templateName);            
        } catch (IOException e) {
            e.printStackTrace();
            throw new IOException();
        }
	}
	
	public <T> String generate (Template template, T t) throws IOException, TemplateException{
		Map<String, Object> templateData = new HashMap<>();
		templateData.put("object", t);
		
		try {
			StringWriter out = new StringWriter();

		    template.process(templateData, out);
		    
		    out.flush();
		    
		    return out.getBuffer().toString().replaceAll("\\r\\n", "").replaceAll("\\t", "");
		    
	    }catch (IOException e) {
	        e.printStackTrace();
	        throw new IOException();
	    } catch (TemplateException e) {
	        e.printStackTrace();
	        throw new TemplateException(null);
	    }
		
	}
	
}
