//package ar.gov.mendoza.salud.ipsmendoza.backup;
//
//import ca.uhn.fhir.context.FhirContext;
//import ca.uhn.fhir.rest.gclient.StringClientParam;
//import ar.gov.mendoza.salud.ipsmendoza.clients.MPIClient;
//import ar.gov.mendoza.salud.ipsmendoza.mpi.dao.local.PatientLocal;
//import ar.gov.mendoza.salud.ipsmendoza.mpi.dao.local.Name;
//import ar.gov.mendoza.salud.ipsmendoza.repository.BundleRepository;
//import org.hl7.fhir.r4.formats.JsonCreator;
//import org.hl7.fhir.r4.model.*;
//import org.hl7.fhir.r4.model.Address.AddressUse;
//import org.hl7.fhir.r4.model.AllergyIntolerance.AllergyIntoleranceCriticality;
//import org.hl7.fhir.r4.model.AllergyIntolerance.AllergyIntoleranceReactionComponent;
//import org.hl7.fhir.r4.model.AllergyIntolerance.AllergyIntoleranceSeverity;
//import org.hl7.fhir.r4.model.AllergyIntolerance.AllergyIntoleranceType;
//import org.hl7.fhir.r4.model.ContactPoint.ContactPointSystem;
//import org.hl7.fhir.r4.model.ContactPoint.ContactPointUse;
//import org.hl7.fhir.r4.model.Device.DeviceDeviceNameComponent;
//import org.hl7.fhir.r4.model.Device.DeviceNameType;
//import org.hl7.fhir.r4.model.Immunization.ImmunizationStatus;
//import org.hl7.fhir.r4.model.Medication.MedicationStatus;
//import org.hl7.fhir.r4.model.MedicationStatement.MedicationStatementStatus;
//import org.hl7.fhir.r4.model.Narrative.NarrativeStatus;
//import org.hl7.fhir.r4.model.codesystems.DataAbsentReason;
//import org.hl7.fhir.utilities.xhtml.NodeType;
//import org.hl7.fhir.utilities.xhtml.XhtmlNode;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//
//import java.text.SimpleDateFormat;
//import java.time.LocalDate;
//import java.time.LocalTime;
//import java.util.ArrayList;
//import java.util.Base64;
//import java.util.Date;
//import java.text.ParseException;
//import java.util.List;
//
//import javax.xml.crypto.Data;
//
///**
// * Created by Scaravelli on 21/05/19.
// */
//@RestController
//public class HsaludControllerBackup {
//
//    @Autowired
//
//    private BundleRepository bundleRepository;
//
//    private static final String REST_URI = "http://mpisaludprod.mendoza.gov.ar:8080/mpi-fhir/fhir/Patient/";
//    private final String[] meses = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};
//    // https://bus.msal.gov.ar/ /v1/DocumentReference?
//    // subject:Patient.identifier=http://salud.mendoza.gov.ar|992116&class=https://loinc.org/|60591-5
///*
//    @GetMapping("/DocumentReference/{identifier}")
//    public String getBundleDocumentReference(@PathVariable(value = "identifier") String identifier) {
//
//        Bundle bundle=new Bundle();
//        bundle.setId("1111111");
//
//
//        FhirContext ctxR4 = FhirContext.forR4();
//        String resp = ctxR4.newJsonParser().encodeResourceToString(bundle);
//
//        return resp;
//    }*/
//    //private Client client = ClientBuilder.newClient();
//    public PatientLocal getPatient(int id_patient) {
//
//        MPIClient mpiClient = new MPIClient();
//        PatientLocal patientLocal = mpiClient.findPatientLocal(id_patient);
//
//       /*
//        WebClient webClient = WebClient.create("http://mpisaludprod.mendoza.gov.ar:8080/");
//        Mono<String> result = webClient.get()
//                .uri()
//                .retrieve()
//                .bodyToMono(String.class);
//        String response = result.block();
//        System.out.println(response);
//       */
//        return  patientLocal;
//    }
//    
//    @GetMapping("/DocumentReference/{id}")
//    public String getBundleDocumentReference(@RequestParam ("Patient.identifier") String identifier,@RequestParam ("class") String classtipo) {
//
//        Bundle bundle=new Bundle();
//        //Ver como definir id del bundle
//        bundle.setId("1c89b971-1be2-4b03-960d-56836c711cff");
//        bundle.setType(Bundle.BundleType.SEARCHSET);
//
//        DocumentReference documentReference=new DocumentReference();
//
//        List<Bundle.BundleEntryComponent> listaentry= new ArrayList();
//
//        Bundle.BundleEntryComponent entry00 = new Bundle.BundleEntryComponent();
//        entry00.setFullUrl("http://fhir.hl7fundamentals.org/r4/DocumentReference/xxxx");
//        entry00.setResource(documentReference);
//        listaentry.add(entry00);
//
//        bundle.setEntry(listaentry);
//
//        FhirContext ctxR4 = FhirContext.forR4();
//        String resp1 = ctxR4.newJsonParser().setPrettyPrint(true).encodeResourceToString(bundle);
//
//        return resp1;
//    }
//
//    @GetMapping("/Binary/{id}")
//    public String getBinary (@PathVariable(value = "id") String binaryId) {
//
//        //Bundle bundle=this.getIps(binaryId);
//        String bundle=this.getIps(binaryId);
//
//         FhirContext ctxR4 = FhirContext.forR4();
//         //String bundlestr = ctxR4.newJsonParser().encodeResourceToString(bundle);
//
//        Binary binary = new Binary();
//        binary.setContentType("application/json");
//        ///Es el id provincial del MPI Mendoza??
//        binary.setId(binaryId);
//
//        String encodedString = Base64.getEncoder().encodeToString(bundle.getBytes());
//
//        binary.setData(encodedString.getBytes());
//
//        //binary.setData(patients.getBytes());
//
//        String resp = ctxR4.newJsonParser().setPrettyPrint(true).encodeResourceToString(binary);
//
//        return resp;
//    }
//
//    @GetMapping("/Bundle/{id}")
//    public String getIps (@PathVariable(value = "id") String ipsId) {
//
//        Bundle bundle=new Bundle();
//        bundle.setId("Resumen-HC-IPS-SaludDigital.ar");
//
//        Meta meta=new Meta();
//        meta.setLastUpdated(new Date());
//        bundle.setLanguage("es-AR");
//
//        bundle.setMeta(meta);
//        bundle.setType(Bundle.BundleType.DOCUMENT);
//
//        Identifier identifier= new Identifier();
//        //identifier.setSystem("2.16.840.1.113883.2.10.28"); /*oid mendoza ?*/
//        identifier.setSystem("http://salud.mendoza.gov.ar");
//        identifier.setValue(ipsId);
//
//        bundle.setIdentifier(identifier);
//
//        ////////////////////////////////////////////
//
//        Composition composition=this.construirComposition(identifier);
//
//        Patient patient1=this.construirPatient(identifier);
//        
//        // PatientBuilder patientBuilder = new PatientBuilder(1004);
//        // Patient patient = patientBuilder.construirPatient();
//        //
//        // Bundle.BundleEntryComponent entryPatient = new Bundle.BundleEntryComponent();
//        // entry11.setFullUrl("http://hapi.fhir.org/baseR4/Patient/IPS-examples-Patient-01");
//        // entry11.setResource(patient);
//        // listaentry.add(entryPatient);
//        //
//
//        Organization organization=this.construirOrganization(identifier);
//
//        Condition condition=this.construirCondition(identifier);
//
//        MedicationStatement medication = this.construirMedication(identifier);
//        
//        AllergyIntolerance allergyIntolerance = this.construirAllergyIntolerance(identifier);
//        
//        Immunization immunization = this.construirImmunization(identifier);
//        
//        Device device = this.construirDevice(identifier);
//
//        List<Bundle.BundleEntryComponent> listaentry= new ArrayList();
//        
//        // patientBuilder.addIps(listaentry);
//        // listaentry.add(patientBuilder.construirPatient);
//        // listaentry.add(conditionBuilder.construirCondition);
//
//        Bundle.BundleEntryComponent entry00 = new Bundle.BundleEntryComponent();
//        entry00.setFullUrl("http://hapi.fhir.org/baseR4/Composition/IPS-examples-Composition-01");
//        entry00.setResource(composition);
//        listaentry.add(entry00);
//
//        Bundle.BundleEntryComponent entry11 = new Bundle.BundleEntryComponent();
//        entry11.setFullUrl("http://hapi.fhir.org/baseR4/Patient/IPS-examples-Patient-01");
//        entry11.setResource(patient1);
//        listaentry.add(entry11);
//
//        Bundle.BundleEntryComponent entry12 = new Bundle.BundleEntryComponent();
//        entry12.setFullUrl("http://hapi.fhir.org/baseR4/Organization/IPS-examples-Organization-01");
//        entry12.setResource(organization);
//        listaentry.add(entry12);
//
//        Bundle.BundleEntryComponent entry13 = new Bundle.BundleEntryComponent();
//        entry13.setFullUrl("http://hapi.fhir.org/baseR4/Condition/IPS-examples-Condition-01");
//        entry13.setResource(condition);
//        listaentry.add(entry13);
//
//        Bundle.BundleEntryComponent entry55 = new Bundle.BundleEntryComponent();
//        entry55.setFullUrl("http://hapi.fhir.org/baseR4/MedicationStatement/IPS-examples-MedicationStatement-01");
//        entry55.setResource(medication);
//        listaentry.add(entry55);
//
//        Bundle.BundleEntryComponent entry16 = new Bundle.BundleEntryComponent();
//        entry16.setFullUrl("http://fhir.msal.gov.ar/AllergyIntolerance/Ejemplos-IPS-AR-AllergyIntolerance-01");
//        entry16.setResource(allergyIntolerance);
//        listaentry.add(entry16);
//        
//        Bundle.BundleEntryComponent entry17 = new Bundle.BundleEntryComponent();
//        entry17.setFullUrl("http://fhir.msal.gov.ar/Device/Ejemplos-IPS-AR-Device-01");
//        entry17.setResource(device);
//        listaentry.add(entry17);
//        
//        Bundle.BundleEntryComponent entry18 = new Bundle.BundleEntryComponent();
//        entry18.setFullUrl("http://hapi.fhir.org/baseR4/Immunization/IPS-examples-Immunization-01");
//        entry18.setResource(immunization);
//        listaentry.add(entry18);
//
//        bundle.setEntry(listaentry);
//
//        FhirContext ctxR4 = FhirContext.forR4();
//        String bundles = ctxR4.newJsonParser().setPrettyPrint(true).encodeResourceToString(bundle);
//
//        return bundles;
//    }    
//
//    public Composition construirComposition(Identifier identifier){
//	    ///////////////RECURSOS////////////////////////////////
//	    Composition composition= new Composition();
//	
//	    composition.setId("IPS-Composition-01");
//	    CodeableConcept codeable=new CodeableConcept();
//	
//	    Coding cod4=new Coding();
//	    cod4.setCode("60591-5");
//	    cod4.setDisplay("Patient Summary");
//	    cod4.setSystem("http://loinc.org");
//	
//	    codeable.addCoding(cod4);
//	
//	    composition.setType(codeable);
//	    composition.setConfidentiality(Composition.DocumentConfidentiality.N);
//	  
//	    //TODO: Revisar implementación de fecha y hora
//	    LocalDate fechaActual = LocalDate.now();
//	    LocalTime horaActual = LocalTime.now();    
//	    int dia = fechaActual.getDayOfMonth();
//	    String mes = meses[fechaActual.getMonthValue()-1];
//	    int año = fechaActual.getYear();
//	    int hora = horaActual.getHour();
//	    int minuto = horaActual.getMinute();    
//	    String actual = "Resumen del paciente al "+dia+" de "+mes+" de "+año+", "+hora+":"+minuto+"hs";    
//	    // Antiguo hardcode: composition.setTitle("Resumen del paciente al 18 de Febrero de 2019, 14:00hs");
//	    composition.setTitle(actual);
//	    //Fin title
//	    
//	    composition.setStatus(Composition.CompositionStatus.fromCode("final"));
//	    composition.setDate(new Date());
//	
//	    Reference ref=new Reference();
//	    composition.setSubject(ref.setReference("http://hapi.fhir.org/baseR4/Patient/IPS-examples-Patient-01"));
//	
//	    List<Reference> listRef=new ArrayList<>();
//	    Reference author=new Reference();
//	    author.setReference("http://hapi.fhir.org/baseR4/Device/IPS-examples-Device-01");
//	    listRef.add(author);
//	
//	    composition.setAuthor(listRef);
//	
//	    composition.setIdentifier(identifier);
//	
//	    String profile="http://hl7.org/fhir/uv/ips/StructureDefinition/composition-uv-ips";
//	
//	    Meta meta1=new Meta();
//	    meta1.addProfile(profile);
//	
//	    composition.setMeta(meta1);
//	
//	    Narrative narrative=new Narrative();
//	    narrative.setStatus(Narrative.NarrativeStatus.GENERATED);
//	    XhtmlNode xtmlnode1=new XhtmlNode();
//	    xtmlnode1.setValue("<div xmlns=\"http://www.w3.org/1999/xhtml\"><p><b>Generated Narrative with Details</b></p></div>");
//	    narrative.setDiv(xtmlnode1);
//	
//	    composition.setText(narrative);
//	
//	    composition.setCustodian(new Reference().setReference("http://hapi.fhir.org/baseR4/Organization/IPS-examples-Organization-01"));
//	
//	    List<Composition.CompositionAttesterComponent> listCompatt=new ArrayList<>();
//	    Composition.CompositionAttesterComponent attester=new Composition.CompositionAttesterComponent();
//	    attester.setMode(Composition.CompositionAttestationMode.LEGAL);
//	    attester.setTime(new Date());
//	    attester.setParty(new Reference().setReference("http://hapi.fhir.org/baseR4/Organization/IPS-examples-Organization-01"));
//	    listCompatt.add(attester);
//	
//	    composition.setAttester(listCompatt);
//	    //******************
//	
//	    List<Composition.SectionComponent> listaseccion= new ArrayList<>();
//	//Section 0
//	    //parametros
//	    String narrative_div="<div xmlns=\"http://www.w3.org/1999/xhtml\"><p><b>Generated Narrative with Details</b></p></div>";
//	    String codeable_code = "11450-4";
//	    String codeable_display = "Problem list";
//	    String codeable_system = "http://loinc.org";
//	    String lisref1 = "http://hapi.fhir.org/baseR4/Condition/IPS-examples-Condition-01";
//	    String sectitulo = "Problemas";
//	
//	    Composition.SectionComponent seccion0 = construirSeccion0(narrative_div,codeable_code,codeable_display,codeable_system,lisref1,sectitulo);
//	//******************
//	    //Section 1
//	    //parametros
//	    String narrative_div1="<div xmlns=\"http://www.w3.org/1999/xhtml\">\n    <table>\n                                <thead>\n                                    <tr>\n                                        <th>Medicamento</th>\n                                        <th>Strength</th>\n                                        <th>Forma</th>\n                                        <th>Dosis</th>\n                                        <th>Comentario</th>\n                                    </tr>\n                                </thead>\n                                <tbody>\n                                    <tr>\n                                        <td>salbutamol (sustancia)</td>\n                                        <td>200 mcg</td>\n                                        <td>disparo</td>\n                                        <td>uno por día</td>\n                                        <td>tratamiento de asma</td>\n                                    </tr>\n                                                          </tbody>\n                            </table>\n                        </div>";
//	    String codeable_code1 = "10160-0";
//	    String codeable_display1 = "Medication use";
//	    String codeable_system1 = "http://loinc.org";
//	    String lisref11 = "http://hapi.fhir.org/baseR4/MedicationStatement/IPS-examples-MedicationStatement-01";
//	    String sectitulo1= "Medicamentos";
//	    Composition.SectionComponent seccion1= construirSeccion0(narrative_div1,codeable_code1,codeable_display1,codeable_system1,lisref11,sectitulo1);
//	//******************
//	    //Section 2
//	    //parametros
//	    String narrative_div2="<div xmlns=\"http://www.w3.org/1999/xhtml\">Alergia a penicilina (trastorno), criticidad alta, activo</div>";
//	    String codeable_code2 = "48765-2";
//	    String codeable_display2 = "Allergies and/or adverse reactions";
//	    String codeable_system2 = "http://loinc.org";
//	    String lisref12 = "http://hapi.fhir.org/baseR4/AllergyIntolerance/IPS-examples-AllergyIntolerance-01";
//	    String sectitulo2 = "Alergias";
//	    Composition.SectionComponent seccion2=construirSeccion0(narrative_div2,codeable_code2,codeable_display2,codeable_system2,lisref12,sectitulo2);
//	//******************
//		  
//		//Section 3
//	    //TODO: Revisar
//		//parametros
//	    String narrative_div3="<div xmlns=\"http://www.w3.org/1999/xhtml\">\r\n" + 
//	    		"                            <table>\r\n" + 
//	    		"                                <thead>\r\n" + 
//	    		"                                    <tr>\r\n" + 
//	    		"                                        <td>Descripción</td>\r\n" + 
//	    		"                                        <td>Estado</td>\r\n" + 
//	    		"                                        <td>Fecha</td>\r\n" + 
//	    		"                                        <td>Protocolo/Serie</td>\r\n" + 
//	    		"                                        <td>Organización</td>\r\n" + 
//	    		"                                        <td>Ubicación</td>\r\n" + 
//	    		"                                    </tr>\r\n" + 
//	    		"                                </thead>\r\n" + 
//	    		"                                <tbody>\r\n" + 
//	    		"                                    <tr>\r\n" + 
//	    		"                                        <td>Vacuna antigripal trivalente influenza A H1N1, H3 +\r\n" + 
//	    		"                                            influenza B</td>\r\n" + 
//	    		"                                        <td>Completo</td>\r\n" + 
//	    		"                                        <td>2017</td>\r\n" + 
//	    		"                                        <td>Antigripal Esquema Personal de Salud/1</td>\r\n" + 
//	    		"                                        <td>Hospital Municipal Hospital Doctor Ángel Pintos</td>\r\n" + 
//	    		"                                        <td>Hospital Municipal Hospital Doctor Ángel Pintos</td>\r\n" + 
//	    		"                                    </tr>\r\n" + 
//	    		"                                </tbody>\r\n" + 
//	    		"                            </table>\r\n" + 
//	    		"                        </div>";
//	    String codeable_code3 = "60484-3";
//	    String codeable_display3 = "Immunization record";
//	    String codeable_system3 = "http://loinc.org";
//	    String lisref3 = "http://fhir.msal.gov.ar/Immunization/Ejemplos-IPS-AR-Immunization-01";
//	    String sectitulo3 = "Vacunas";
//	    Composition.SectionComponent seccion3 = construirSeccion0(narrative_div3, codeable_code3, codeable_display3, codeable_system3, lisref3, sectitulo3);
//	    
//	//******************
//	   
//	    listaseccion.add(seccion0);
//	    listaseccion.add(seccion1);
//	    listaseccion.add(seccion2);
//	    listaseccion.add(seccion3);
//	
//	    composition.setSection(listaseccion);
//	
//	    return composition;
//	
//	}
//
//public Composition.SectionComponent construirSeccion0(String narrative_div,String codeable_code,String codeable_display,String codeable_system,String lisref1,String sectitulo){
//
//    Composition.SectionComponent seccion= new Composition.SectionComponent();
//
//    Narrative narrative1=new Narrative();
//    narrative1.setStatus(Narrative.NarrativeStatus.GENERATED);
//    XhtmlNode xtmlnode2=new XhtmlNode();
//    xtmlnode2.setValue(narrative_div);
//    narrative1.setDiv(xtmlnode2);
//
//    CodeableConcept codeable1=new CodeableConcept();
//    Coding cod01=new Coding();
//    cod01.setCode(codeable_code);
//    cod01.setDisplay(codeable_display);
//    cod01.setSystem(codeable_system);
//    codeable1.addCoding(cod01);
//
//    List<Reference> listRefSection0=new ArrayList<>();
//    Reference refsection0=new Reference();
//    refsection0.setReference(lisref1);
//    listRefSection0.add(refsection0);
//
//    seccion.setText(narrative1);
//    seccion.setCode(codeable1);
//    seccion.setTitle(sectitulo);
//    seccion.setEntry(listRefSection0);
//
//    return seccion;
//
//}
//	public Patient construirPatient(Identifier identifier){
//
//	    PatientLocal patientLocal=this.getPatient(Integer.parseInt(identifier.getValue()));
//	    System.out.println("Respuesta :"+patientLocal);
//	
//	    Patient patient=new Patient();
//	    patient.setId("IPS-examples-Patient-01");
//	    patient.setGender(Enumerations.AdministrativeGender.valueOf(patientLocal.getGender().toUpperCase()));
//	    /*FECHA NACIMIENTO*******************/
//	    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
//	    String fechanac= patientLocal.getBirthDate();
//	    Date fechanac1 = new Date();
//	    try {
//	
//	         fechanac1 = formatter.parse(fechanac);
//	    } catch (ParseException e) {
//	        e.printStackTrace();
//	    }
//	    /********************/
//	    patient.setBirthDate(fechanac1);
//	
//	   //Contactos, falta agregar a clases de Patient origen ->pi.dao.local.PatientLocal
//	    /*
//	    List<ContactPoint> listaContactos=new ArrayList<ContactPoint>();
//	    ContactPoint contacto=new ContactPoint();
//	
//	    contacto.setValue("99999");
//	    contacto.setSystem(ContactPoint.ContactPointSystem.PHONE);
//	    listaContactos.add(contacto);
//	
//	   patient.setTelecom(listaContactos);
//	   */
//	    patient.setActive(patientLocal.getActive());
//	    //patient.setTelecom();
//	    /*Name****************************/
//	    List<Name> names = patientLocal.getName();
//	    patient.setName(toRemoteName(names));
//	
//	    List<ar.gov.mendoza.salud.ipsmendoza.mpi.dao.local.Identifier> listaidentificadores = patientLocal.getIdentifier();
//	    patient.setIdentifier(toRemoteIdentifier(listaidentificadores,patientLocal.getId().toString()));
//	
//	    /*
//	        "telecom": [
//	          ,
//	            "system": "phone",
//	            "value": "44318113"
//	          }
//	        ],*/
//	
//	    return patient;
//    }
//	
//	//Hola soy un cambio
//
//    public Organization construirOrganization(Identifier identifier){
//        Organization organization=new Organization();
//
//        organization.setId("IPS-examples-Organization-01");
//        organization.setName("Hospital Antonio J. Scaravelli");
//        organization.setActive(true);
//        
//        
//        //Inicio Address
//        
//        //Lista de direcciones
//        List<Address> addressList = new ArrayList<Address>();
//        //Direccion simple a ser rellenada
//        Address address = new Address();
//        
//        //Atributos simples
//        address.setUse(AddressUse.WORK);
//        address.setCountry("Argentina");
//        address.setCity("Tunuyan");
//        address.setPostalCode("5560");
//        
//        //Lineas
//        List<StringType> lines = new ArrayList<StringType>();
//        StringType line = new StringType("Martin Miguel de Guemes 1441");
//        lines.add(line);        
//        address.setLine(lines);
//        
//        //Se agrega direccion a la lista de direcciones
//        addressList.add(address);
//        
//        organization.setAddress(addressList);
//        
//        //Fin Address
//        
//        //Inicio Telecom
//        
//        //Lista de puntos de contactos
//        List<ContactPoint> contactPoints = new ArrayList<ContactPoint>();
//        //Punto de contacto
//        ContactPoint contactPoint = new ContactPoint();
//        
//        contactPoint.setSystem(ContactPointSystem.PHONE);
//        contactPoint.setValue("+54 02622 422324");
//        contactPoint.setUse(ContactPointUse.WORK);
//        
//        //Se agrega contacto a la lista de contactos
//        contactPoints.add(contactPoint);
//        //Se agrega telecom al organization
//        organization.setTelecom(contactPoints);        
//        
//        //Fin Telecom
//        
//        //Inicio Text
//        
//        Narrative narrative = new Narrative();
//        narrative.setStatus(NarrativeStatus.EMPTY);
//        
//        XhtmlNode xhtmlNode = new XhtmlNode();
//        xhtmlNode.setNodeType(NodeType.Text);
//        xhtmlNode.setValue("empty");   
//        narrative.setDiv(xhtmlNode);
//        organization.setText(narrative);
//        
//        //Fin Text
//        
//        //Inicio Identifier
//        
//        //Lista de identifiers
//        List<Identifier> identifiers = new ArrayList<Identifier>();
//        
//        //Identificador del refes
//        Identifier refes = new Identifier();
//        refes.setSystem("http://argentina.gob.ar/salud/refes");
//        refes.setValue("10501192157034");
//        identifiers.add(refes);
//        
//        //Identificador del dominio
//        Identifier dominio = new Identifier();
//        dominio.setSystem("http://argentina.gob.ar/salud/bus-interoperabilidad/dominio");
//        dominio.setValue("http://salud.mendoza.gov.ar");
//        identifiers.add(dominio);
//        
//        organization.setIdentifier(identifiers);
//        
//        //Fin Identifier
//        
//        return organization;
//    }
//    
//    public Condition construirCondition(Identifier identifier){
//
//        Condition condition=new Condition();
//
//        condition.setId("IPS-examples-Condition-01");
//        
//        //Inicio Text
//        
//        Narrative narrative = new Narrative();
//        narrative.setStatus(NarrativeStatus.EMPTY);
//        
//        condition.setText(narrative);        
//        
//        //Fin Text
//        
//        //Inicio ClinicalStatus
//        
//        CodeableConcept codeableClinicalStatus = new CodeableConcept();
//        Coding codeClinicalStatus = new Coding();
//        codeClinicalStatus.setSystem("http://terminology.hl7.org/CodeSystem/condition-clinical");
//        codeClinicalStatus.setCode("active");
//        codeableClinicalStatus.addCoding(codeClinicalStatus);
//        
//        condition.setClinicalStatus(codeableClinicalStatus);
//        
//        //Final ClinicalStatus
//        
//        //Inicio VerificationStatus
//        
//        CodeableConcept codeableVerificationStatus = new CodeableConcept();
//        Coding codeVerificationStatus = new Coding();
//        codeVerificationStatus.setSystem("http://terminology.hl7.org/CodeSystem/condition-ver-status");
//        codeVerificationStatus.setCode("confirmed");
//        codeableVerificationStatus.addCoding(codeVerificationStatus);
//        
//        condition.setVerificationStatus(codeableVerificationStatus);
//        
//        //Fin VerificationStatus
//        
//        //Inicio Category
//        
//        List<CodeableConcept> conditionCategories = new ArrayList<CodeableConcept>();
//        CodeableConcept conditionCategory = new CodeableConcept();
//        
//        List<Coding> codeCategories = new ArrayList<Coding>();
//        Coding codeCategory = new Coding();
//        codeCategory.setSystem("http://loinc.org");
//        codeCategory.setCode("75326-9");
//        codeCategory.setDisplay("Problem");
//        codeCategories.add(codeCategory);        
//        
//        conditionCategories.add(conditionCategory);
//        
//        conditionCategory.setCoding(codeCategories);
//        
//        condition.setCategory(conditionCategories);
//        
//        //Fin Category
//        
//        //Inicio Code
//        
//        CodeableConcept codeableCondition = new CodeableConcept();
//        
//        List<Coding> codingConditions = new ArrayList<Coding>();
//        Coding codingCondition = new Coding();
//        codingCondition.setSystem("http://hl7.org/fhir/uv/ips/CodeSystem/absent-unknown-uv-ips");
//        codingCondition.setCode("no-problem-info");
//        codingCondition.setDisplay("No known problems");
//        codingConditions.add(codingCondition);
//        
//        codeableCondition.setCoding(codingConditions);
//        
//        condition.setCode(codeableCondition);
//        
//        //Fin Code
//        
//        //Inicio Subject
//        
//        Reference referenceCondition = new Reference();
//        referenceCondition.setReference("http://fhir.msal.gov.ar/Patient/Ejemplos-IPS-AR-Patient-01");
//        condition.setSubject(referenceCondition);
//        
//        //Fin Subject
//        
//        //Inicio onsetDateTime
//        
//        //TODO: Averiguar acerca de la construcción del onsetTime
//        //condition.setOnset(value)
//        
//        List<Extension> extensionConditions = new ArrayList<Extension>();
//        Extension extensionCondition = new Extension();
//        extensionCondition.setUrl("http://hl7.org/fhir/StructureDefinition/data-absent-reason");
//        
//        CodeType codeCondition = new CodeType();
//        codeCondition.setValue("unknown");
//        extensionCondition.setValue(codeCondition);
//        extensionConditions.add(extensionCondition);
//        
//        DateTimeType conditionOnSet = new DateTimeType();
//        conditionOnSet.setExtension(extensionConditions);
//        
//        condition.setOnset(conditionOnSet);
//                
//        //Fin onsetDateTime
//
//        return condition;
//    }
//    
//    public Immunization construirImmunization(Identifier identifier) {
//    	
//    	//Recurso a devolver
//    	Immunization immunization = new Immunization();
//    	
//    	immunization.setId("IPS-examples-Immunization-01");
//    	
//    	immunization.setStatus(ImmunizationStatus.COMPLETED);
//    	
//    	//Inicio VaccineCode
//    	
//    	CodeableConcept codeableVaccineCode = new CodeableConcept();
//    	List<Coding> codingVaccines = new ArrayList<Coding>();
//    	
//    	Coding codingVaccine = new Coding();    
//    	codingVaccine.setSystem("http://hl7.org/fhir/uv/ips/CodeSystem/absent-unknown-uv-ips");
//    	codingVaccine.setCode("no-immunization-info");
//    	codingVaccine.setDisplay("no-immunization-info");
//    	codingVaccines.add(codingVaccine);
//    	    	
//    	codeableVaccineCode.setCoding(codingVaccines);
//    	
//    	immunization.setVaccineCode(codeableVaccineCode);
//    	
//    	//Fin VaccineCode
//    	
//    	//Inicio Patient
//    	
//    	Reference immunizationPatientReference = new Reference();
//    	immunizationPatientReference.setReference("http://hapi.fhir.org/baseR4/Patient/IPS-examples-Patient-01");
//    	    	
//    	immunization.setPatient(immunizationPatientReference);
//    	
//    	//Fin Patient
//    	
//    	//Inicio OcurrenceDateTime
//    	
//    	Extension extension = new Extension();
//    	extension.setUrl("http://hl7.org/fhir/StructureDefinition/data-absent-reason");    	
//    	CodeType valueCode = new CodeType();
//    	valueCode.setValue("unknown");    	
//    	extension.setValue(valueCode);
//    	
//    	DateTimeType dtType = new DateTimeType();
//    	dtType.addExtension(extension);
//    	
//    	immunization.setOccurrence(dtType);
//    	
//    	//Fin OcurrenceDateTime
//    	
//    	
//    	return immunization;
//    	
//    }
//    
//    public MedicationStatement construirMedication(Identifier identifier){
//
//        MedicationStatement medication = new MedicationStatement();
//
//        medication.setId("IPS-examples-MedicationStatement-01");
//        
//        medication.setStatus(MedicationStatementStatus.ACTIVE);
//        
//        //Inicio MedicationCodeableConcept
//        
//        CodeableConcept codeableMedication = new CodeableConcept();        
//        List<Coding> codingMedications = new ArrayList<Coding>();
//        
//        Coding codingMeditacion = new Coding();
//        codingMeditacion.setSystem("http://hl7.org/fhir/uv/ips/CodeSystem/absent-unknown-uv-ips");
//        codingMeditacion.setCode("no-medication-info");
//        codingMeditacion.setDisplay("No information about current medications");
//        codingMedications.add(codingMeditacion);
//        
//        codeableMedication.setCoding(codingMedications);
//        
//        medication.setMedication(codeableMedication);
//                
//        //Fin MedicationCodeableConcept
//        
//        //Inicio EffectivePeriod
//        
//        Period period = new Period();
//        
//        Extension medicationExtension = new Extension();
//        medicationExtension.setUrl("http://hl7.org/fhir/StructureDefinition/data-absent-reason");        
//        CodeType valueCode = new CodeType();        
//    	valueCode.setValue("unknown");    	    	
//        medicationExtension.setValue(valueCode);
//        period.addExtension(medicationExtension);
//        
//        medication.setEffective(period);
//        
//        //Fin EffectivePeriod
//        
//        //Inicio Subject
//        
//        Reference reference = new Reference();
//        reference.setReference("http://hapi.fhir.org/baseR4/Patient/IPS-examples-Patient-01");
//        medication.setSubject(reference);
//        
//        //Fin Subject
//        
//        //Inicio Meta
//        
//        Meta meta = new Meta();
//        List<CanonicalType> profiles = new ArrayList<CanonicalType>();
//        CanonicalType profile = new CanonicalType();
//        
//        profile.setValue("http://hl7.org/fhir/uv/ips/StructureDefinition/medicationstatement-uv-ips");
//        profiles.add(profile);
//        
//        meta.setProfile(profiles);
//        medication.setMeta(meta);
//        
//        //Fin Meta
//        
//        //Inicio Text        
//        
//        Narrative narrative = new Narrative();
//        narrative.setStatus(NarrativeStatus.EMPTY);
//        
//        XhtmlNode xhtmlNode = new XhtmlNode();
//        xhtmlNode.setNodeType(NodeType.Text);
//        xhtmlNode.setValue("empty");   
//        narrative.setDiv(xhtmlNode);
//        
//        medication.setText(narrative);
//        
//        //Fin Text
//        
//
//        return medication;
//    }  
//    
//    // IMPLEMENTACIÓN DE RECURSOS
//    // TODO: REVISAR
//    
//    public AllergyIntolerance construirAllergyIntolerance(Identifier identifier) {
//    	
//    	AllergyIntolerance allergyIntolerance = new AllergyIntolerance();
//    	
//    	allergyIntolerance.setId("IPS-examples-AllergyIntolerance-01");
//    	
//    	//Inicio Type    	
//    	//Alternativas, implementar condicionales: ALLERGY, INTOLERANCE, NULL
//    	//allergyIntolerance.setType(AllergyIntoleranceType.ALLERGY);    	
//    	//Fin Type    	
//    	
//    	//Inicio Criticality    	
//    	//Alternativas, implementar condicionales: HIGH, LOW, UNABLETOASSESS, NULL
//    	//allergyIntolerance.setCriticality(AllergyIntoleranceCriticality.HIGH);    	
//    	//Fin Criticality 
//    	
//    	//Inicio Code    	
//    	
//    	CodeableConcept codeableConcept = new CodeableConcept();
//    	Coding coding = new Coding();
//    	coding.setSystem("http://hl7.org/fhir/uv/ips/CodeSystem/absent-unknown-uv-ips");
//    	coding.setCode("no-allergy-info");   	
//    	codeableConcept.addCoding(coding);    	
//    	allergyIntolerance.setCode(codeableConcept);    
//    	
//    	//Fin Code   
//    	
//    	//Inicio patient    	
//    	Reference patientReference = new Reference();
//    	patientReference.setReference("http://fhir.msal.gov.ar/Patient/Ejemplos-IPS-AR-Patient-01");    	
//    	allergyIntolerance.setPatient(patientReference);    	
//    	//Fin patient      	
//    	
//    	//Inicio onsetDateTime    	
//    	//age.setValue(10000);
//    	//DateType inicio = new DateType();
//    	//inicio.setDay(16);
//    	//inicio.setMonth(9);
//    	//inicio.setYear(2019);
//    	//DateType inicio = new DateType(2019, 9, 16);    	
//    	//LocalDate inicioAllergy = LocalDate.of(2014, 6, 30);    	
//    	//allergyIntolerance.getOnsetDateTimeType().setValue(DataAbsentReason.NOTAPPLICABLE);
//    	//allergyIntolerance.getOnsetDateTimeType().setValueAsString(DataAbsentReason.NOTAPPLICABLE.toString());
//    	//allergyIntolerance.getOnsetDateTimeType().setVa    	
//    	//Fin onsetDateTime    	
//    	
//    	//Inicio reaction
//    	//TODO: Investigar Reaction
//    	AllergyIntoleranceReactionComponent reaction = new AllergyIntoleranceReactionComponent();
//    	
//    	CodeableConcept manifestation = new CodeableConcept();
//    	
//    	Coding manifestationCoding = new Coding();
//    	manifestationCoding.setSystem("http://snomed.info/sct");
//    	manifestationCoding.setCode("716186003");
//    	manifestationCoding.setDisplay("Sin alergia conocida (situación)");
//    	manifestation.addCoding(manifestationCoding);    	
//    	reaction.addManifestation(manifestation);   
//    	    	
//    	reaction.setSeverity(AllergyIntoleranceSeverity.NULL);
//    	
//    	allergyIntolerance.addReaction(reaction);
//    	//Fin reaction
//    	
//    	return allergyIntolerance;
//    	
//    }
//    
//    public Device construirDevice(Identifier identifier) {
//    	
//    	Device device = new Device();
//    	
//    	device.setId("IPS-examples-Devices-01");    	
//    	
//    	//Inicio Identificadores 
//    	List<Identifier> identifiers = new ArrayList<Identifier>();
//    	
//    	//TODO: Verificar origen del identifier
//    	
//    	//for para recorrer diferentes identificadores
//    	Identifier identifierDevice1 = new Identifier();
//    	identifierDevice1.setSystem("http://salud.mendoza.gov.ar/Device");
//    	identifierDevice1.setValue("http://mpisaludprod.mendoza.gov.ar");
//    	identifiers.add(identifierDevice1);
//    	//fin bucle for
//    	
//    	device.setIdentifier(identifiers);
//    	//Fin Identificadores 
//    	
//    	//Inicio deviceName  
//    	
//    	//for para recorrer diferentes deviceName
//    	DeviceDeviceNameComponent ddNameComponent = new DeviceDeviceNameComponent();
//    	ddNameComponent.setName("Sistema MPI Provincial Mendoza");
//    	ddNameComponent.setType(DeviceNameType.MANUFACTURERNAME);
//    	device.addDeviceName(ddNameComponent);
//    	//fin bucle for
//    	
//    	//Fin deviceName
//    	
//    	//Inicio Type
//    	
//    	CodeableConcept codeableConcept = new CodeableConcept();
//    	
//    	//for para recorrer todos los codings
//    	Coding coding = new Coding();
//    	coding.setSystem("http://snomed.info/sct");
//    	coding.setCode("462894001");
//    	coding.setDisplay("software de aplicación de sistema de información de historias clínicas de pacientes (objeto físico)");
//    	codeableConcept.addCoding(coding);
//    	//fin bucle for
//    	
//    	device.setType(codeableConcept);
//    	
//    	//Fin type
//    	
//    	//Inicio owner
//    	
//    	Reference reference = new Reference();
//    	//TODO: Revisar ya que difiere el JSON provisto por Nación de la documentación de SIMPLIFIER.NET
//    	reference.setReference("http://fhir.msal.gov.ar/refes/Organization/14999912399913");
//    	device.setOwner(reference);
//    	
//    	//Fin owner
//    	
//    	return device;
//    	
//    }
//    
//    // FIN DE IMPLEMENTACIÓN   
//    
//
///*
//    private List<org.hl7.fhir.r4.model.Identifier> toRemoteIdentifier(List<ar.gov.mendoza.salud.ipsmendoza.mpi.dao.local.Identifier> localIds) {
//        List<org.hl7.fhir.r4.model.Identifier> remoteIds = new ArrayList<>();
//
//        for (ar.gov.mendoza.salud.ipsmendoza.mpi.dao.local.Identifier localId : localIds) {
//
//            org.hl7.fhir.r4.model.Identifier identifier = new org.hl7.fhir.r4.model.Identifier();
//            identifier.setSystem(localId.getSystem());
//            identifier.setValue(localId.getValue());
//            identifier.setPeriod(toRemotePeriod(localId.getPeriod()));
//            identifier.setUse(Identifier.IdentifierUse.USUAL);
//            //usual es para renaper y official para los id de todos los efectores y del federador
//            //el periodo (start) se agregar para todos los identificadores, excepto renaper (usual)
//            remoteIds.add(identifier);
//        }
//
//        return remoteIds;
//    }
//
// */
//
//    private List<Identifier> toRemoteIdentifier(List<ar.gov.mendoza.salud.ipsmendoza.mpi.dao.local.Identifier> localIds,String mpiId) {
//
//        List<org.hl7.fhir.r4.model.Identifier> remoteIds = new ArrayList<>();
//
//        for (ar.gov.mendoza.salud.ipsmendoza.mpi.dao.local.Identifier localId : localIds) {
//
//            if (localId.getPeriod().getEnd().equals("0001-01-01")) {
//                Identifier identifier =new Identifier();
//                if (localId.getSystem().equals("http://salud.mendoza.gov.ar/tiposdocumentos/dni")){
//
//                    identifier.setSystem("http://www.renaper.gob.ar/dni");
//
//                } //TODO: mapear otra cosa ¿?
//                else if (localId.getSystem().equals("")) {
//
//                }
//                if (identifier.getSystem()!=null && !identifier.getSystem().equals("")) {
//                    identifier.setValue(localId.getValue());
//                    identifier.setPeriod(toRemotePeriod(localId.getPeriod()));
//                    remoteIds.add(identifier);
//                }
//            }
//        }
//
//        //Identificador Provincial
//        Identifier identifierProvincial = new Identifier();
//
//        // identifierProvincial.setSystem("urn:oid:2.16.840.1.113883.2.10.28");
//        identifierProvincial.setSystem("http://salud.mendoza.gov.ar");
//        identifierProvincial.setValue(mpiId);
//        //identifierProvincial.setPeriod(new Period("2019-04-16"));
//        remoteIds.add(identifierProvincial);
//        return remoteIds;
//    }
//
//    private org.hl7.fhir.r4.model.Period toRemotePeriod(ar.gov.mendoza.salud.ipsmendoza.mpi.dao.local.Period localPeriod) {
//        /*FECHA NACIMIENTO*******************/
//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
//        String periodo= localPeriod.getStart();
//        Date dateperiodo = new Date();
//        try {
//
//            dateperiodo = formatter.parse(periodo);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//
//        Period period = new Period();
//        period.setStart(dateperiodo);
//        //TODO: Verificar por que que el periodo remoto no posee fin (método end())
//        //period.setEnd(localPeriod.getEnd());
//        return period;
//    }
//
//    private List<org.hl7.fhir.r4.model.HumanName> toRemoteName(List<Name> localName) {
//
//        List<HumanName> lista=new ArrayList<HumanName>();
//
//        List<String> listaFamily =localName.get(0).getFamily();
//        List<String> listaGiven =localName.get(0).getGiven();
//        //org.hl7.fhir.r4.model.StringType
//
//        String primerNombre=listaGiven.get(0).trim();
//        String segundoNombre="";
//        List<StringType> listaGGiven = new ArrayList<StringType>();
//        listaGGiven.add(new StringType(primerNombre.trim()));
//
//        if (listaGiven.size()>1 && listaGiven.get(1)!=null){
//            // System.out.println ("listaGiven.size: " + listaGiven.size());
//            segundoNombre=" "+listaGiven.get(1).trim();
//            listaGGiven.add(new StringType(segundoNombre.trim()));
//
//        }
//
//        String primerApellido=listaFamily.get(0).trim();
//        String segundoApellido="";
//        if (listaFamily.size()>1 && listaFamily.get(1)!=null){
//            segundoApellido=" "+listaFamily.get(1).trim();
//        }
//
//        HumanName rNname = new HumanName();
//
//         rNname.setText(primerNombre+segundoNombre+" "+primerApellido+segundoApellido);
//
//
//        rNname.setGiven(listaGGiven);
//        rNname.setUse(HumanName.NameUse.OFFICIAL);
//
//
//        StringType family =new StringType(primerApellido+segundoApellido);
//        family.addExtension().setUrl("https://federador.msal.gob.ar/primer_apellido").setValue(new StringType(primerApellido.trim()));
//        if (listaFamily.size()>1 && listaFamily.get(1)!=null) {
//            family.addExtension().setUrl("https://federador.msal.gob.ar/segundo_apellido").setValue(new StringType(segundoApellido.trim()));
//        }
//        rNname.setFamilyElement(family);
//
//       lista.add(rNname);
//
//        return lista;
//    }
//
//    //Ejemplo de uso de FHIR
//    @GetMapping("/Patient/{id}")
//    public String getPatient (@PathVariable(value = "id") String patientId) {
//
//    	Identifier identifier= new Identifier();
//        //identifier.setSystem("2.16.840.1.113883.2.10.28"); /*oid mendoza ?*/
//        identifier.setSystem("http://salud.mendoza.gov.ar");
//        identifier.setValue(patientId);
//    	
//    	Patient patient=this.construirPatient(identifier);
//    	
//    	
//        // ...populate...
//        //patient.addIdentifier().setSystem("urn:mrns").setValue("12345");
//        //patient.addName().setFamily("Smith").addGiven("Tester").addGiven("Q");
//
//        FhirContext ctxR4 = FhirContext.forR4();
//        String resp = ctxR4.newJsonParser().setPrettyPrint(true).encodeResourceToString(patient);
//
//        return resp;
//        
//    }
//
//}
