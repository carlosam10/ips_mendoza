package ar.gov.mendoza.salud.ipsmendoza.mpi.dao.builder;

import java.util.List;

import org.hl7.fhir.r4.model.Bundle.BundleEntryComponent;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.MedicationStatement;
import org.hl7.fhir.r4.model.Reference;
import org.hl7.fhir.r4.model.AllergyIntolerance.AllergyIntoleranceCriticality;
import org.hl7.fhir.r4.model.AllergyIntolerance.AllergyIntoleranceReactionComponent;
import org.hl7.fhir.r4.model.AllergyIntolerance.AllergyIntoleranceSeverity;
import org.hl7.fhir.r4.model.AllergyIntolerance;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.CodeableConcept;
import org.hl7.fhir.r4.model.Coding;

public class AllergyIntoleranceBuilder implements Builder<AllergyIntolerance, Object> {

	private Identifier identifier;

	@Override
	public AllergyIntolerance construir(Identifier identifier) {
		AllergyIntolerance allergyIntolerance = new AllergyIntolerance();

		allergyIntolerance.setId("1");

		// Inicio Type
		// Alternativas, implementar condicionales: ALLERGY, INTOLERANCE, NULL
		// allergyIntolerance.setType(AllergyIntoleranceType.ALLERGY);
		// Fin Type

		// Inicio Criticality
		// Alternativas, implementar condicionales: HIGH, LOW, UNABLETOASSESS, NULL
		// allergyIntolerance.setCriticality(AllergyIntoleranceCriticality.HIGH);
		// Fin Criticality

		// Inicio Code

		CodeableConcept codeableConcept = new CodeableConcept();
		Coding coding = new Coding();
		coding.setSystem("http://hl7.org/fhir/uv/ips/CodeSystem/absent-unknown-uv-ips");
		coding.setCode("no-allergy-info");
		coding.setDisplay("No hay información de alergias");
		codeableConcept.addCoding(coding);
		allergyIntolerance.setCode(codeableConcept);

		// Fin Code
		
		allergyIntolerance.setCriticality(AllergyIntoleranceCriticality.UNABLETOASSESS);
		
		// Inicio patient
		Reference patientReference = new Reference();
		patientReference.setReference("https://apisalud.mendoza.gov.ar/api/ips/fhir/Patient/1");
		allergyIntolerance.setPatient(patientReference);
		// Fin patient

		// Inicio onsetDateTime
		// age.setValue(10000);
		// DateType inicio = new DateType();
		// inicio.setDay(16);
		// inicio.setMonth(9);
		// inicio.setYear(2019);
		// DateType inicio = new DateType(2019, 9, 16);
		// LocalDate inicioAllergy = LocalDate.of(2014, 6, 30);
		// allergyIntolerance.getOnsetDateTimeType().setValue(DataAbsentReason.NOTAPPLICABLE);
		// allergyIntolerance.getOnsetDateTimeType().setValueAsString(DataAbsentReason.NOTAPPLICABLE.toString());
		// allergyIntolerance.getOnsetDateTimeType().setVa
		// Fin onsetDateTime

		// Inicio reaction
		// TODO: Investigar Reaction
		AllergyIntoleranceReactionComponent reaction = new AllergyIntoleranceReactionComponent();

		CodeableConcept manifestation = new CodeableConcept();

		Coding manifestationCoding = new Coding();
		manifestationCoding.setSystem("http://snomed.info/sct");
		manifestationCoding.setCode("716186003");
		manifestationCoding.setDisplay("Sin alergia conocida (situación)");
		manifestation.addCoding(manifestationCoding);
		reaction.addManifestation(manifestation);

		reaction.setSeverity(AllergyIntoleranceSeverity.NULL);

		allergyIntolerance.addReaction(reaction);
		// Fin reaction

		return allergyIntolerance;
	}

	@Override
	public Object obtener() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void agregarEntries(List<BundleEntryComponent> entries) {
		Bundle.BundleEntryComponent entry16 = new Bundle.BundleEntryComponent();
		entry16.setFullUrl("https://apisalud.mendoza.gov.ar/api/ips/fhir/AllergyIntolerance/1");
		entry16.setResource(construir(identifier));
		entries.add(entry16);
	}

}
