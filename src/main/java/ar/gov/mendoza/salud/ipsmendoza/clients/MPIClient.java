package ar.gov.mendoza.salud.ipsmendoza.clients;
import ar.gov.mendoza.salud.ipsmendoza.mpi.dao.local.PatientLocal;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;


public class MPIClient {
    private static final String REST_URI = "http://mpisaludprod.mendoza.gov.ar:8080/mpi-fhir/fhir/Patient/";

    private Client client = ClientBuilder.newClient();
    private ObjectMapper mapper = new ObjectMapper();

    public MPIClient() {
        client.target(REST_URI).request().accept(MediaType.APPLICATION_JSON);
    }

    public String getJson(int id) {
        Response response = client.target(REST_URI).path(String.valueOf(id)).request(MediaType.APPLICATION_JSON).get();
        String s = response.readEntity(String.class);
        return  s;
    }

    public PatientLocal findPatientLocal(int id) {
        String jsonPatientLocal = getJson(id);
        PatientLocal patientLocal = null;
        try {
            patientLocal = mapper.readValue(jsonPatientLocal, PatientLocal.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return patientLocal;
    }
}
