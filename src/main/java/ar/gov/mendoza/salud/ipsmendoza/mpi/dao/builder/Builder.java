package ar.gov.mendoza.salud.ipsmendoza.mpi.dao.builder;

import java.util.List;

import org.hl7.fhir.r4.model.Bundle.BundleEntryComponent;
import org.hl7.fhir.r4.model.DomainResource;
import org.hl7.fhir.r4.model.Identifier;

public interface Builder<FHIR extends DomainResource, DAO> {

	public FHIR construir(Identifier identifier) throws Exception;
	
	public DAO obtener() throws Exception;
	
	public void agregarEntries(List<BundleEntryComponent> entries) throws Exception;
	
}
