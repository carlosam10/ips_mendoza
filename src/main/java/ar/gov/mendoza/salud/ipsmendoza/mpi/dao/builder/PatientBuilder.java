package ar.gov.mendoza.salud.ipsmendoza.mpi.dao.builder;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hl7.fhir.r4.model.Bundle.BundleEntryComponent;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Enumerations;
import org.hl7.fhir.r4.model.HumanName;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Narrative;
import org.hl7.fhir.r4.model.Narrative.NarrativeStatus;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.Period;
import org.hl7.fhir.r4.model.StringType;
import org.hl7.fhir.utilities.xhtml.NodeType;
import org.hl7.fhir.utilities.xhtml.XhtmlNode;

import ar.gov.mendoza.salud.ipsmendoza.clients.MPIClient;
import ar.gov.mendoza.salud.ipsmendoza.html.GenerateHTML;
import ar.gov.mendoza.salud.ipsmendoza.mpi.dao.local.Name;
import ar.gov.mendoza.salud.ipsmendoza.mpi.dao.local.PatientLocal;
import freemarker.template.TemplateException;

public class PatientBuilder implements Builder<Patient, PatientLocal> {

	private Identifier identifier;

	public PatientBuilder(Identifier identifier) {

		this.identifier = identifier;

	}

	public Patient construir(Identifier identifier) throws Exception {

		PatientLocal patientLocal;
		
		try {
			patientLocal = this.obtener();
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}

		Patient patient = new Patient();
		patient.setId("1");
		patient.setGender(Enumerations.AdministrativeGender.valueOf(patientLocal.getGender().toUpperCase()));
		/* FECHA NACIMIENTO *******************/
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String fechanac = patientLocal.getBirthDate();
		Date fechanac1 = new Date();
		try {

			fechanac1 = formatter.parse(fechanac);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		/********************/
		patient.setBirthDate(fechanac1);

		// Contactos, falta agregar a clases de Patient origen
		// ->pi.dao.local.PatientLocal
		/*
		 * List<ContactPoint> listaContactos=new ArrayList<ContactPoint>(); ContactPoint
		 * contacto=new ContactPoint();
		 * 
		 * contacto.setValue("99999");
		 * contacto.setSystem(ContactPoint.ContactPointSystem.PHONE);
		 * listaContactos.add(contacto);
		 * 
		 * patient.setTelecom(listaContactos);
		 */
		patient.setActive(patientLocal.getActive());
		// patient.setTelecom();
		/* Name ****************************/
		List<Name> names = patientLocal.getName();
		patient.setName(toRemoteName(names));

		List<ar.gov.mendoza.salud.ipsmendoza.mpi.dao.local.Identifier> listaidentificadores = patientLocal
				.getIdentifier();
		patient.setIdentifier(toRemoteIdentifier(listaidentificadores, patientLocal.getId().toString()));

		/*
		 * "telecom": [ , "system": "phone", "value": "44318113" } ],
		 */

		GenerateHTML html = new GenerateHTML();

		Narrative narrative = new Narrative();

		String value = "nada";

		try {
			value = html.generate(html.chargeTemplate("patient.ftl"), patient);
		} catch (IOException | TemplateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		narrative.setStatus(NarrativeStatus.GENERATED);
		
		XhtmlNode xhtmlNode = new XhtmlNode();
		xhtmlNode.setNodeType(NodeType.Text);
		xhtmlNode.setValue(value);
		narrative.setDiv(xhtmlNode);

		patient.setText(narrative);

		return patient;

	}

	public PatientLocal obtener() throws Exception {

		try {
			MPIClient mpiCliente = new MPIClient();
			PatientLocal patientLocal = mpiCliente.findPatientLocal(Integer.parseInt(identifier.getValue()));
			return patientLocal;
		} catch (Exception e) {
			throw new Exception("No se puede acceder a los datos del servidor. Problema de Acceso a recurso Patient.");
		}

	}

	public void agregarEntries(List<BundleEntryComponent> entries) throws Exception {

		try {
			
			Bundle.BundleEntryComponent entry = new Bundle.BundleEntryComponent();
			entry.setFullUrl("https://apisalud.mendoza.gov.ar/api/ips/fhir/Patient/1");
			entry.setResource(construir(identifier));
			entries.add(entry);
			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}

	}

	private List<Identifier> toRemoteIdentifier(List<ar.gov.mendoza.salud.ipsmendoza.mpi.dao.local.Identifier> localIds,
			String mpiId) {

		List<org.hl7.fhir.r4.model.Identifier> remoteIds = new ArrayList<>();

		for (ar.gov.mendoza.salud.ipsmendoza.mpi.dao.local.Identifier localId : localIds) {

			if (localId.getPeriod().getEnd().equals("0001-01-01")) {
				Identifier identifier = new Identifier();
				if (localId.getSystem().equals("http://salud.mendoza.gov.ar/tiposdocumentos/dni")) {

					identifier.setSystem("http://www.renaper.gob.ar/dni");

				} // TODO: mapear otra cosa ¿?
				else if (localId.getSystem().equals("")) {

				}
				if (identifier.getSystem() != null && !identifier.getSystem().equals("")) {
					identifier.setValue(localId.getValue());
					identifier.setPeriod(toRemotePeriod(localId.getPeriod()));
					remoteIds.add(identifier);
				}
			}
		}

		// Identificador Provincial
		Identifier identifierProvincial = new Identifier();

		// identifierProvincial.setSystem("urn:oid:2.16.840.1.113883.2.10.28");
		identifierProvincial.setSystem("http://salud.mendoza.gov.ar");
		identifierProvincial.setValue(mpiId);
		// identifierProvincial.setPeriod(new Period("2019-04-16"));
		remoteIds.add(identifierProvincial);
		return remoteIds;
	}

	private org.hl7.fhir.r4.model.Period toRemotePeriod(
			ar.gov.mendoza.salud.ipsmendoza.mpi.dao.local.Period localPeriod) {
		/* FECHA NACIMIENTO *******************/
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String periodo = localPeriod.getStart();
		Date dateperiodo = new Date();
		try {

			dateperiodo = formatter.parse(periodo);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		Period period = new Period();
		period.setStart(dateperiodo);
		// TODO: Verificar por que que el periodo remoto no posee fin (método end())
		// period.setEnd(localPeriod.getEnd());
		return period;
	}

	private List<org.hl7.fhir.r4.model.HumanName> toRemoteName(List<Name> localName) {

		List<HumanName> lista = new ArrayList<HumanName>();

		List<String> listaFamily = localName.get(0).getFamily();
		List<String> listaGiven = localName.get(0).getGiven();
		// org.hl7.fhir.r4.model.StringType

		String primerNombre = listaGiven.get(0).trim();
		String segundoNombre = "";
		List<StringType> listaGGiven = new ArrayList<StringType>();
		listaGGiven.add(new StringType(primerNombre.trim()));

		if (listaGiven.size() > 1 && listaGiven.get(1) != null) {
			// System.out.println ("listaGiven.size: " + listaGiven.size());
			segundoNombre = " " + listaGiven.get(1).trim();
			listaGGiven.add(new StringType(segundoNombre.trim()));

		}

		String primerApellido = listaFamily.get(0).trim();
		String segundoApellido = "";
		if (listaFamily.size() > 1 && listaFamily.get(1) != null) {
			segundoApellido = " " + listaFamily.get(1).trim();
		}

		HumanName rNname = new HumanName();

		rNname.setText(primerNombre + segundoNombre + " " + primerApellido + segundoApellido);

		rNname.setGiven(listaGGiven);
		rNname.setUse(HumanName.NameUse.OFFICIAL);

		StringType family = new StringType(primerApellido + segundoApellido);
		family.addExtension().setUrl("https://federador.msal.gob.ar/primer_apellido")
				.setValue(new StringType(primerApellido.trim()));
		if (listaFamily.size() > 1 && listaFamily.get(1) != null) {
			family.addExtension().setUrl("https://federador.msal.gob.ar/segundo_apellido")
					.setValue(new StringType(segundoApellido.trim()));
		}
		rNname.setFamilyElement(family);

		lista.add(rNname);

		return lista;
	}

}
