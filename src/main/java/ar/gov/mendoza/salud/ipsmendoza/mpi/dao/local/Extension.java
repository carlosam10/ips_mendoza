
package ar.gov.mendoza.salud.ipsmendoza.mpi.dao.local;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.HashMap;
import java.util.Map;

@JsonIgnoreProperties({"additionalProperties"})
public class Extension {

    private String url;
    private String valueString;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Extension() {
    }

    /**
     * 
     * @param valueString
     * @param url
     */
    public Extension(String url, String valueString) {
        super();
        this.url = url;
        this.valueString = valueString;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getValueString() {
        return valueString;
    }

    public void setValueString(String valueString) {
        this.valueString = valueString;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
