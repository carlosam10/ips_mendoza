# Project IPS-Mendoza. 

Spring Rest APIs. HAPI FHIR  

## Requirements

1. Java - 1.8.x

2. Maven - 3.x.x

3. Spring 2.0

## Steps to Setup

**1. Clone the application**

```bash
git clone  https://carlosam10@bitbucket.org/carlosam10/ips_mendoza.git
```
